package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellService;

public class SellService implements ISellService {
	
	public double computeSellingPrice(Car c) {
		double purchasePrice = c.getPrice();
		double year = c.getYear();
		
		return purchasePrice - (purchasePrice / 7)*(2015-year);
	}
}
