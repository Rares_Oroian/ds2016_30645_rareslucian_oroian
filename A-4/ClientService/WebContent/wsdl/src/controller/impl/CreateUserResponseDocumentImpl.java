/*
 * An XML document type.
 * Localname: createUserResponse
 * Namespace: http://controller
 * Java type: controller.CreateUserResponseDocument
 *
 * Automatically generated - do not modify.
 */
package controller.impl;
/**
 * A document containing one createUserResponse(@http://controller) element.
 *
 * This is a complex type.
 */
public class CreateUserResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.CreateUserResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public CreateUserResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREATEUSERRESPONSE$0 = 
        new javax.xml.namespace.QName("http://controller", "createUserResponse");
    
    
    /**
     * Gets the "createUserResponse" element
     */
    public controller.CreateUserResponseDocument.CreateUserResponse getCreateUserResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.CreateUserResponseDocument.CreateUserResponse target = null;
            target = (controller.CreateUserResponseDocument.CreateUserResponse)get_store().find_element_user(CREATEUSERRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "createUserResponse" element
     */
    public void setCreateUserResponse(controller.CreateUserResponseDocument.CreateUserResponse createUserResponse)
    {
        generatedSetterHelperImpl(createUserResponse, CREATEUSERRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "createUserResponse" element
     */
    public controller.CreateUserResponseDocument.CreateUserResponse addNewCreateUserResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.CreateUserResponseDocument.CreateUserResponse target = null;
            target = (controller.CreateUserResponseDocument.CreateUserResponse)get_store().add_element_user(CREATEUSERRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML createUserResponse(@http://controller).
     *
     * This is a complex type.
     */
    public static class CreateUserResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.CreateUserResponseDocument.CreateUserResponse
    {
        private static final long serialVersionUID = 1L;
        
        public CreateUserResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        
    }
}
