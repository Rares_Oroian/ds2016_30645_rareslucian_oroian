/*
 * An XML document type.
 * Localname: loginResponse
 * Namespace: http://controller
 * Java type: controller.LoginResponseDocument
 *
 * Automatically generated - do not modify.
 */
package controller.impl;
/**
 * A document containing one loginResponse(@http://controller) element.
 *
 * This is a complex type.
 */
public class LoginResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.LoginResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public LoginResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LOGINRESPONSE$0 = 
        new javax.xml.namespace.QName("http://controller", "loginResponse");
    
    
    /**
     * Gets the "loginResponse" element
     */
    public controller.LoginResponseDocument.LoginResponse getLoginResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.LoginResponseDocument.LoginResponse target = null;
            target = (controller.LoginResponseDocument.LoginResponse)get_store().find_element_user(LOGINRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "loginResponse" element
     */
    public void setLoginResponse(controller.LoginResponseDocument.LoginResponse loginResponse)
    {
        generatedSetterHelperImpl(loginResponse, LOGINRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "loginResponse" element
     */
    public controller.LoginResponseDocument.LoginResponse addNewLoginResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.LoginResponseDocument.LoginResponse target = null;
            target = (controller.LoginResponseDocument.LoginResponse)get_store().add_element_user(LOGINRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML loginResponse(@http://controller).
     *
     * This is a complex type.
     */
    public static class LoginResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.LoginResponseDocument.LoginResponse
    {
        private static final long serialVersionUID = 1L;
        
        public LoginResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName LOGINRETURN$0 = 
            new javax.xml.namespace.QName("http://controller", "loginReturn");
        
        
        /**
         * Gets the "loginReturn" element
         */
        public int getLoginReturn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LOGINRETURN$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "loginReturn" element
         */
        public org.apache.xmlbeans.XmlInt xgetLoginReturn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInt target = null;
                target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(LOGINRETURN$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "loginReturn" element
         */
        public void setLoginReturn(int loginReturn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LOGINRETURN$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LOGINRETURN$0);
                }
                target.setIntValue(loginReturn);
            }
        }
        
        /**
         * Sets (as xml) the "loginReturn" element
         */
        public void xsetLoginReturn(org.apache.xmlbeans.XmlInt loginReturn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInt target = null;
                target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(LOGINRETURN$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(LOGINRETURN$0);
                }
                target.set(loginReturn);
            }
        }
    }
}
