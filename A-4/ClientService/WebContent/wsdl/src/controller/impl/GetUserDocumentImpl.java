/*
 * An XML document type.
 * Localname: getUser
 * Namespace: http://controller
 * Java type: controller.GetUserDocument
 *
 * Automatically generated - do not modify.
 */
package controller.impl;
/**
 * A document containing one getUser(@http://controller) element.
 *
 * This is a complex type.
 */
public class GetUserDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.GetUserDocument
{
    private static final long serialVersionUID = 1L;
    
    public GetUserDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETUSER$0 = 
        new javax.xml.namespace.QName("http://controller", "getUser");
    
    
    /**
     * Gets the "getUser" element
     */
    public controller.GetUserDocument.GetUser getGetUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.GetUserDocument.GetUser target = null;
            target = (controller.GetUserDocument.GetUser)get_store().find_element_user(GETUSER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getUser" element
     */
    public void setGetUser(controller.GetUserDocument.GetUser getUser)
    {
        generatedSetterHelperImpl(getUser, GETUSER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "getUser" element
     */
    public controller.GetUserDocument.GetUser addNewGetUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.GetUserDocument.GetUser target = null;
            target = (controller.GetUserDocument.GetUser)get_store().add_element_user(GETUSER$0);
            return target;
        }
    }
    /**
     * An XML getUser(@http://controller).
     *
     * This is a complex type.
     */
    public static class GetUserImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.GetUserDocument.GetUser
    {
        private static final long serialVersionUID = 1L;
        
        public GetUserImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName USERNAME$0 = 
            new javax.xml.namespace.QName("http://controller", "username");
        
        
        /**
         * Gets the "username" element
         */
        public java.lang.String getUsername()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "username" element
         */
        public org.apache.xmlbeans.XmlString xgetUsername()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(USERNAME$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "username" element
         */
        public void setUsername(java.lang.String username)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(USERNAME$0);
                }
                target.setStringValue(username);
            }
        }
        
        /**
         * Sets (as xml) the "username" element
         */
        public void xsetUsername(org.apache.xmlbeans.XmlString username)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(USERNAME$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(USERNAME$0);
                }
                target.set(username);
            }
        }
    }
}
