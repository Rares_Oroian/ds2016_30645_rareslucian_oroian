/*
 * An XML document type.
 * Localname: getUserResponse
 * Namespace: http://controller
 * Java type: controller.GetUserResponseDocument
 *
 * Automatically generated - do not modify.
 */
package controller.impl;
/**
 * A document containing one getUserResponse(@http://controller) element.
 *
 * This is a complex type.
 */
public class GetUserResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.GetUserResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public GetUserResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETUSERRESPONSE$0 = 
        new javax.xml.namespace.QName("http://controller", "getUserResponse");
    
    
    /**
     * Gets the "getUserResponse" element
     */
    public controller.GetUserResponseDocument.GetUserResponse getGetUserResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.GetUserResponseDocument.GetUserResponse target = null;
            target = (controller.GetUserResponseDocument.GetUserResponse)get_store().find_element_user(GETUSERRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "getUserResponse" element
     */
    public void setGetUserResponse(controller.GetUserResponseDocument.GetUserResponse getUserResponse)
    {
        generatedSetterHelperImpl(getUserResponse, GETUSERRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "getUserResponse" element
     */
    public controller.GetUserResponseDocument.GetUserResponse addNewGetUserResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            controller.GetUserResponseDocument.GetUserResponse target = null;
            target = (controller.GetUserResponseDocument.GetUserResponse)get_store().add_element_user(GETUSERRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML getUserResponse(@http://controller).
     *
     * This is a complex type.
     */
    public static class GetUserResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements controller.GetUserResponseDocument.GetUserResponse
    {
        private static final long serialVersionUID = 1L;
        
        public GetUserResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GETUSERRETURN$0 = 
            new javax.xml.namespace.QName("http://controller", "getUserReturn");
        
        
        /**
         * Gets the "getUserReturn" element
         */
        public entities.User getGetUserReturn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                entities.User target = null;
                target = (entities.User)get_store().find_element_user(GETUSERRETURN$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "getUserReturn" element
         */
        public void setGetUserReturn(entities.User getUserReturn)
        {
            generatedSetterHelperImpl(getUserReturn, GETUSERRETURN$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "getUserReturn" element
         */
        public entities.User addNewGetUserReturn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                entities.User target = null;
                target = (entities.User)get_store().add_element_user(GETUSERRETURN$0);
                return target;
            }
        }
    }
}
