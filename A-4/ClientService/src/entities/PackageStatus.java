package entities;

import java.util.Date;

public class PackageStatus {
	private int id;
	private String city;
	private Date time;
	private int packageId;
	
	public PackageStatus() {}
	
	public PackageStatus(int id, String city, Date time, int packageId) {
		this.id = id;
		this.city = city;
		this.time = time;
		this.packageId = packageId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	@Override
	public String toString() {
		return "PackageStatus [id=" + id + ", city=" + city + ", time=" + time
				+ ", packageId=" + packageId + "]";
	}
}
