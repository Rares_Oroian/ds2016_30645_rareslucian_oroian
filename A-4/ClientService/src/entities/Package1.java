package entities;

public class Package1 {
	private int packageId;
	private int sender;
	private int receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destinationCity;
	private int track;
	
	public Package1() {}
	public Package1(int id, int sender, int receiver, String name, String description, String senderCity, String destinationCity, int track) {
		this.packageId = id;
		this.sender = sender;
		this.receiver = receiver;
		this.name = name;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.track = track;
	}
	
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public int getSender() {
		return sender;
	}
	public void setSender(int sender) {
		this.sender = sender;
	}
	public int getReceiver() {
		return receiver;
	}
	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public int getTrack() {
		return track;
	}
	public void setTrack(int track) {
		this.track = track;
	}
	
	@Override
	public String toString() {
		return "Package [packageId=" + packageId + ", sender=" + sender
				+ ", receiver=" + receiver + ", name=" + name
				+ ", description=" + description + ", senderCity=" + senderCity
				+ ", destinationCity=" + destinationCity + ", track=" + track
				+ "]";
	}
}
