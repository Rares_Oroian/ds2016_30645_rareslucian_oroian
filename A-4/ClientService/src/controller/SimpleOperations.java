package controller;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import entities.Package1;
import entities.PackageStatus;
import entities.User;
import other.JDBCMysqlConnection;


public class SimpleOperations {
	public SimpleOperations() {}
	
	public boolean createUser(String username, String password, int type) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String insertUserSQL = "INSERT INTO user(username, password, type) VALUES(?,?,?)";
		
		try {
			connection = JDBCMysqlConnection.getConnection();
			preparedStatement = connection.prepareStatement(insertUserSQL);
			
			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);
			preparedStatement.setInt(3, type);
			
			preparedStatement.executeUpdate();
			
			return true;
		} catch (SQLException e) {
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return false;
	}
	
	public User getUser(String username) {
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		User user = null;
		
		String query = "SELECT * FROM user WHERE username='" + username + "'";
		
		try {
			connection = JDBCMysqlConnection.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			
			if (rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setType(rs.getInt("type"));
			}
		} catch (SQLException e) {
			
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return user;
	}
	
	public int login(String username, String password) {
		int response = 0;
		
		User user = getUser(username);
		
		if (user != null) {
			if (user.getPassword().equals(password)) {
				response = user.getType();
			}
			
			System.out.println(user.toString());
		} else {
			System.out.println("User is null");
		}
		
		return response;
	}
	
	public List<Package1> listAllPackages(int clientId) {
		List<Package1> list = new ArrayList<Package1>();
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		
		String query = "SELECT * FROM package WHERE sender=" + clientId + " or receiver=" + clientId;

		System.out.println(query);
		
		try {
			connection = JDBCMysqlConnection.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			
			
			while (rs.next()) {
				Package1 p = new Package1(
						rs.getInt(1),
						rs.getInt(2),
						rs.getInt(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7),
						rs.getInt(8)
				);
				
				list.add(p);
			}
		} catch (SQLException e) {
			
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(list.toString());
		
		return list;
	}
	
	public Package1 getPackage(int packageId) {
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		Package1 p = null;
		
		String query = "SELECT * FROM package WHERE idPackage=" + packageId;
		
		System.out.println(query);
		
		try {
			connection = JDBCMysqlConnection.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			
			if (rs.next()) {
				p = new Package1(
					rs.getInt(1),
					rs.getInt(2),
					rs.getInt(3),
					rs.getString(4),
					rs.getString(5),
					rs.getString(6),
					rs.getString(7),
					rs.getInt(8)
				);
			}
		} catch (SQLException e) {
			
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(p);
		
		return p;
	}
	
	public PackageStatus getPackageStatus(int packageId) {
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		PackageStatus status = null;
		
		String query = "SELECT * FROM route WHERE package=" + packageId;
		
		try {
			connection = JDBCMysqlConnection.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			
			while (rs.next()) {
				status = new PackageStatus(
					rs.getInt(1),
					rs.getString(2),
					rs.getDate(3),
					rs.getInt(4)
				);
			}
		} catch (SQLException e) {
			
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return status;
	}
}
