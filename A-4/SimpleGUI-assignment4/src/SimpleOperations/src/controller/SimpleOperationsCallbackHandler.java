/**
 * SimpleOperationsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:47:34 BST)
 */
package SimpleOperations.src.controller;


/**
 *  SimpleOperationsCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class SimpleOperationsCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public SimpleOperationsCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public SimpleOperationsCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for getUser method
     * override this method for handling normal response from getUser operation
     */
    public void receiveResultgetUser(
        SimpleOperations.src.controller.SimpleOperationsStub.GetUserResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getUser operation
     */
    public void receiveErrorgetUser(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for login method
     * override this method for handling normal response from login operation
     */
    public void receiveResultlogin(
        SimpleOperations.src.controller.SimpleOperationsStub.LoginResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from login operation
     */
    public void receiveErrorlogin(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for listAllPackages method
     * override this method for handling normal response from listAllPackages operation
     */
    public void receiveResultlistAllPackages(
        SimpleOperations.src.controller.SimpleOperationsStub.ListAllPackagesResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from listAllPackages operation
     */
    public void receiveErrorlistAllPackages(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getPackageStatus method
     * override this method for handling normal response from getPackageStatus operation
     */
    public void receiveResultgetPackageStatus(
        SimpleOperations.src.controller.SimpleOperationsStub.GetPackageStatusResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getPackageStatus operation
     */
    public void receiveErrorgetPackageStatus(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for createUser method
     * override this method for handling normal response from createUser operation
     */
    public void receiveResultcreateUser(
        SimpleOperations.src.controller.SimpleOperationsStub.CreateUserResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from createUser operation
     */
    public void receiveErrorcreateUser(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getPackage method
     * override this method for handling normal response from getPackage operation
     */
    public void receiveResultgetPackage(
        SimpleOperations.src.controller.SimpleOperationsStub.GetPackageResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getPackage operation
     */
    public void receiveErrorgetPackage(java.lang.Exception e) {
    }
}
