import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class DeletePackage extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelPackageid = new JLabel("Enter package id: ");
	private JTextField textPackageId = new JTextField(20);
	private JLabel labelStatus = new JLabel("");
	private JButton deleteButton = new JButton("Delete");
	
	public DeletePackage() {
		super("Delete a package");
		
		JPanel newPanel = new JPanel(new GridBagLayout());
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        newPanel.add(labelPackageid, constraints);
        
        constraints.gridx = 1;
        newPanel.add(textPackageId, constraints);
        
        constraints.gridy = 1;
        newPanel.add(deleteButton, constraints);
        
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        newPanel.add(labelStatus, constraints);
        
        add(newPanel);
        
        pack();
        
        setLocationRelativeTo(null);
	}
	
	public String getPackageId() {
		return textPackageId.getText();
	}
	
	public JButton getDeleteButton() {
		return deleteButton;
	}
	
	public void setStatusText(String text, Boolean isGood) {
    	labelStatus.setText(text);
    	
    	if (isGood) {
    		labelStatus.setForeground(Color.GREEN);
    	} else {
    		labelStatus.setForeground(Color.RED);
    	}
    }
}
