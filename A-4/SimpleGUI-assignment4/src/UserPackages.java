import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class UserPackages extends JFrame {
	JTextArea textArea = new JTextArea("", 20, 160);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserPackages() {
		super("You packages list");
		
		JPanel newPanel = new JPanel();
		        
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        
        JScrollPane scroll = new JScrollPane (textArea);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
              scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        newPanel.add(scroll);
        
        // add the panel to this frame
        add(newPanel);
        
        pack();
        setLocationRelativeTo(null);
	}
	
	public void setText(String text) {
		textArea.setText(text);
	}
}
