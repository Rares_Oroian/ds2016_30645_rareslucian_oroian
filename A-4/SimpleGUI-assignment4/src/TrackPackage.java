import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;


public class TrackPackage extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelPackageId = new JLabel("Package Id: ");
    private JLabel labelCity = new JLabel("City: ");
    private JLabel labelTime = new JLabel("Time: ");
    private JLabel labelStatus = new JLabel("");
    private JTextField textPackageId = new JTextField(20);
    private JTextField textCity = new JTextField(20);
    private JTextField textTime = new JTextField(20);
    JSpinner timeSpinner = new JSpinner( new SpinnerDateModel() );
    JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm:ss");
    
    private JButton buttonUpdate = new JButton("Update");
    

	public TrackPackage() {
		super("Track package");
        
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        newPanel.add(labelPackageId, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textPackageId, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelCity, constraints);
         
        constraints.gridx = 1;
        newPanel.add(textCity, constraints);
        
        timeSpinner.setEditor(timeEditor);
	    timeSpinner.setValue(new Date()); // will only show the current time
	    
	    Date dt = new Date();
	    java.text.SimpleDateFormat sdf = 
	         new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String currentTime = sdf.format(dt);
	    
	    textTime.setText(currentTime);
	    
	    constraints.gridx = 0;
        constraints.gridy = 2;     
        newPanel.add(labelTime, constraints);
         
        constraints.gridx = 1;
        newPanel.add(textTime, constraints);
         
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonUpdate, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(labelStatus, constraints);
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Update Package Status"));
        // add the panel to this frame
        add(newPanel);
         
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        pack();
        setLocationRelativeTo(null);
	}
	
	public String getPackageId() {
		return textPackageId.getText();
	}
	
	public String getCity() {
		return textCity.getText();
	}
	
	public String getTime() {
		return textTime.getText();
	}
	
	public JButton getSubmitButton() {
		return buttonUpdate;
	}
	
	public void setStatusText(String text, Boolean isGood) {
    	labelStatus.setText(text);
    	
    	if (isGood) {
    		labelStatus.setForeground(Color.GREEN);
    	} else {
    		labelStatus.setForeground(Color.RED);
    	}
    }
}
