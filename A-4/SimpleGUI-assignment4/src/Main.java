import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.tempuri.IServiceProxy;

import SimpleOperations.src.controller.*;
import SimpleOperations.src.controller.SimpleOperationsStub.Package1;
import SimpleOperations.src.controller.SimpleOperationsStub.PackageStatus;
import SimpleOperations.src.controller.SimpleOperationsStub.User;

public class Main {
	/**
     * Starting point for the SAAJ - SOAP Client Testing
     */
    public static void main(String args[]) {
    	// set look and feel to the system look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    	runLogin();
    }
    
    private static User loggedUser;
    
    private static void runLogin() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final Login login = new Login();
                login.setVisible(true);
                login.getButtonLogin().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						String username = login.getUsername();
						String password = login.getPassword();
						
						try {
							int userType = loginRequest(username, password);
							
							if (userType > 0) {
								login.setStatusText("Login successful!", true);
								
								if (userType == 1) {
									runAdminBoard();
								} else if (userType == 2) {
									runClientBoard();
								}
							} else {
								login.setStatusText("Invalid credentials!", false);
							}
						} catch (RemoteException e) {
							login.setStatusText("Server error", false);
						}
					}
                	
                });
                
                login.getButtonRegister().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						runRegister();
					}
					
                });
            }
        });
    }
    
    private static int loginRequest(String username, String password) throws RemoteException {
		SimpleOperationsStub stub = new SimpleOperationsStub();
		SimpleOperationsStub.Login request = new SimpleOperationsStub.Login();
		
		request.setUsername(username);
		request.setPassword(password);
		SimpleOperationsStub.LoginResponse response = stub.login(request);
		
		if (response.get_return() > 0) {
			// get user
			SimpleOperationsStub.GetUser req = new SimpleOperationsStub.GetUser();
			req.setUsername(username);
			SimpleOperationsStub.GetUserResponse resp = stub.getUser(req);
			loggedUser = resp.get_return();
		}
		
		return response.get_return();
    }
    
    private static void runRegister() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final Register register = new Register();
                register.setVisible(true);
                register.getButtonRegister().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent event) {
						try {
							boolean status = registerRequest(register.getUsername(), register.getPassword());
							
							if (status) {
								register.setStatusText("Account created!", true);
							} else {
								register.setStatusText("Account already exists!", false);
							}
						} catch (RemoteException e) {
							register.setStatusText("Server error", false);
						}
					}
                });
            }
    	});
    }
    
    private static boolean registerRequest(String username, String password) throws RemoteException {
    	SimpleOperationsStub stub = new SimpleOperationsStub();
		SimpleOperationsStub.CreateUser request = new SimpleOperationsStub.CreateUser();
		request.setUsername(username);
		request.setPassword(password);
		request.setType(2);
		SimpleOperationsStub.CreateUserResponse response = stub.createUser(request);
		
		return response.get_return();
    }
    
    private static void runClientBoard() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final ClientBoard clientBoard = new ClientBoard();
            	clientBoard.setVisible(true);
            	
            	clientBoard.getButtonGetAllMine().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						runUserPackages();
					}
            		
            	});
            	clientBoard.getButtonGetOne().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						runGetPackage();
					}
            		
            	});
            	clientBoard.getButtonGetStatus().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						runGetStatus();
					}
            		
            	});
            }
    	});
    }
    
    private static void runUserPackages() {
    	String packagesList = "";
    	
    	try {
    		Package1[] arr = getUserPackagesRequest();
    		
    		if (arr != null) {
        		for (int i = 0; i < arr.length; i++) {
        			Package1 p = arr[i];
        			
        			
            		packagesList += "Package [packageId=" + p.getPackageId() + ", sender=" + p.getSender()
	            				+ ", receiver=" + p.getReceiver() + ", name=" + p.getName()
	            				+ ", description=" + p.getDescription() + ", senderCity=" + p.getSenderCity()
	            				+ ", destinationCity=" + p.getDestinationCity() + ", track=" + p.getTrack()
	            				+ "]";
            	}
    		}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	final String text = packagesList;
    	
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final UserPackages userPackages = new UserPackages();
            	            	
            	userPackages.setText(text);
            	userPackages.setVisible(true);
            }
    	});
    }
    
    private static Package1[] getUserPackagesRequest() throws RemoteException {
    	SimpleOperationsStub stub = new SimpleOperationsStub();
		SimpleOperationsStub.ListAllPackages request = new SimpleOperationsStub.ListAllPackages();
		request.setClientId(loggedUser.getId());
		SimpleOperationsStub.ListAllPackagesResponse response = stub.listAllPackages(request);
		
		return response.get_return();
    }
    
    private static void runGetPackage() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final GetPackage getPackage = new GetPackage();
            	
            	getPackage.getSubmitButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						String id = getPackage.getPackageId();
						
						
						try {
							Package1 p = getPackageRequest(id);
							
							if (p == null){
								return;
							}
							
							String packageInfo = "packageId=" + p.getPackageId() + "\nsender=" + p.getSender()
		            				+ "\nreceiver=" + p.getReceiver() + "\nname=" + p.getName()
		            				+ "\ndescription=" + p.getDescription() + "\nsenderCity=" + p.getSenderCity()
		            				+ "\ndestinationCity=" + p.getDestinationCity() + "\ntrack=" + p.getTrack();
							
							getPackage.setPackageInfo(packageInfo);
						} catch (RemoteException e) {
							getPackage.setPackageInfo("Package does not exist!");
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
            		
            	});
            	
            	getPackage.setVisible(true);
            }
    	});
    }
    
    private static Package1 getPackageRequest(String packageId) throws RemoteException {
    	SimpleOperationsStub stub = new SimpleOperationsStub();
		SimpleOperationsStub.GetPackage request = new SimpleOperationsStub.GetPackage();
		request.setPackageId(Integer.parseInt(packageId));
		SimpleOperationsStub.GetPackageResponse response = stub.getPackage(request);
		
		return response.get_return();
    }
    
    private static void runGetStatus() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final GetStatus getStatus = new GetStatus();
            	
            	getStatus.getSubmitButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						String packageId = getStatus.getPackageId();
		            	
		            	try {
							PackageStatus s = getStatusRequest(packageId);
							if (s == null) {
								getStatus.setPackageStatus("Package status not found");
								return;
							}
							
							String status = "Packagage status:\nCity: " + s.getCity() + "\nTime: " + s.getTime().toString();
							getStatus.setPackageStatus(status);
						} catch (RemoteException e) {
							e.printStackTrace();
							
							getStatus.setPackageStatus("Package status not found");
						}
					}
            	});
            	
            	getStatus.setVisible(true);
            }
    	});
    }
    
    private static PackageStatus getStatusRequest(String packageId) throws RemoteException {
    	SimpleOperationsStub stub = new SimpleOperationsStub();
		SimpleOperationsStub.GetPackageStatus request = new SimpleOperationsStub.GetPackageStatus();
		request.setPackageId(Integer.parseInt(packageId));
		SimpleOperationsStub.GetPackageStatusResponse response = stub.getPackageStatus(request);
		
		return response.get_return();
    }
    
    private static void runAdminBoard() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final AdminBoard adminBoard = new AdminBoard();
            	adminBoard.setVisible(true);
            	
            	adminBoard.getAddPackageButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						runAddPackage();
					}
            		
            	});
            	adminBoard.getRemovePackageButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						runDeletePackage();
					}
            		
            	});
            	adminBoard.getRegisterPackageButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						runUpdateStatus();
					}
            		
            	});
            	adminBoard.getUpdatePackageButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						runRegisterPackageTrack();
					}
            		
            	});
            }
    	});
    }
    
    private static void runAddPackage() {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final AddPackage addPackage = new AddPackage();
            	addPackage.setVisible(true);
            	addPackage.getSubmitButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent event) {
						try {
							String status = addPackageRequest(addPackage.getPackage());
							
							if (status != null) {
								addPackage.setStatusText("PACKAGE CREATED!", true);								
							} else {
								addPackage.setStatusText("Failed to create package!", false);
							}
						} catch (Exception e) {
							e.printStackTrace();
							addPackage.setStatusText("Failed to create package!", false);
						}
					}
                });
            }
    	});
    }
    
    private static String addPackageRequest(Package1 p) throws RemoteException {
    	IServiceProxy adminService = new IServiceProxy();
    	String status = adminService.addPackage(p.getSender(), p.getReceiver(), p.getName(), p.getDescription(), p.getSenderCity(), p.getDestinationCity(), false);
    	
    	return status;
    }
    
	private static void runDeletePackage() {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final DeletePackage deletePackage = new DeletePackage();
            	deletePackage.setVisible(true);
            	
            	deletePackage.getDeleteButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						try {
							String status = deletePackageRequest(deletePackage.getPackageId());
							
							if (status != null) {
								deletePackage.setStatusText("Package DELETED!", true);								
							} else {
								deletePackage.setStatusText("Failed to delete package!", false);
							}
						} catch (Exception e) {
							deletePackage.setStatusText("Failed to delete package!", false);
						}
					}
            		
            	});
            }
		});
    }
	
	private static String deletePackageRequest(String id) throws Exception {
		Integer packageId = Integer.parseInt(id);
		
		IServiceProxy adminService = new IServiceProxy();
    	String status = adminService.removePackage(packageId);
    	
    	return status;
	}
	
	private static void runRegisterPackageTrack() {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final TrackPackage trackPackage = new TrackPackage();
            	trackPackage.setVisible(true);
            	
            	trackPackage.getSubmitButton().addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						try {
							String status = updatePackageRequest(trackPackage.getPackageId(), trackPackage.getCity(), trackPackage.getTime());
							
							if (status != null) {
								trackPackage.setStatusText("Package track registered!", true);								
							} else {
								trackPackage.setStatusText("Failed to register package track!", false);
							}
						} catch (Exception e) {
							trackPackage.setStatusText("Failed to register package track!", false);
						}
					}
            		
            	});
            }
		});
	}
	
	
	private static String updatePackageRequest(String id, String city, String time) throws RemoteException {
		Integer packageId = Integer.parseInt(id);
		
		IServiceProxy adminService = new IServiceProxy();
		
		System.out.println(packageId.toString() + "," + city + "," + time);
    	String status = adminService.updateStatus(city, time, packageId);
    	
    	return status;
	}
	
	private static void runUpdateStatus() {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	final RegisterForTracking registerPackage = new RegisterForTracking();
            	registerPackage.setVisible(true);
            	
            	registerPackage.getSubmitButton().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						try {
							String status = registerPackageRequest(registerPackage.getPackageId(), registerPackage.getCity(), registerPackage.getTime());
							
							if (status != null) {
								registerPackage.setStatusText("Package registed for tracking!", true);								
							} else {
								registerPackage.setStatusText("Failed to register for tracking!", false);
							}
						} catch (Exception e) {
							registerPackage.setStatusText("Failed to register for tracking!", false);
						}
					}
            		
            	});
            }
		});
	}
	
	private static String registerPackageRequest(String id, String city, String time) throws RemoteException {
		Integer packageId = Integer.parseInt(id);
		
		IServiceProxy adminService = new IServiceProxy();
    	String status = adminService.registerForTracking(packageId, city, time);
    	
    	return status;
	}
}
