import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ClientBoard extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton buttonGetAllMine = new JButton("List your packages");
	private JButton buttonGetOne = new JButton("Get package");
	private JButton buttonGetStatus = new JButton("Check package status");
	
	public ClientBoard() {
		super("Client operations board");
		
		JPanel newPanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonGetAllMine, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonGetOne, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonGetStatus, constraints);
        
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Client operations board"));
        // add the panel to this frame
        add(newPanel);
        
        pack();
        setLocationRelativeTo(null);
	}

	public JButton getButtonGetAllMine() {
		return buttonGetAllMine;
	}

	public JButton getButtonGetOne() {
		return buttonGetOne;
	}

	public JButton getButtonGetStatus() {
		return buttonGetStatus;
	}

	@Override
	public String toString() {
		return "ClientBoard [buttonGetAllMine=" + buttonGetAllMine
				+ ", buttonGetOne=" + buttonGetOne + ", buttonGetStatus="
				+ buttonGetStatus + "]";
	}
}
