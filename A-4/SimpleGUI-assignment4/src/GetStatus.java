import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GetStatus extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JLabel labelPackageid = new JLabel("Enter package id: ");
	private JTextField textPackageId = new JTextField(20);
	private JButton buttonGet = new JButton("Get package info");
	JTextArea textArea = new JTextArea("", 20, 20);
	
	public GetStatus() {
		super("Get package status");
		
		JPanel newPanel = new JPanel(new GridBagLayout());
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        newPanel.add(labelPackageid, constraints);
        
        constraints.gridx = 1;
        newPanel.add(textPackageId, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonGet, constraints);
        
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        
        JScrollPane scroll = new JScrollPane (textArea);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
              scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        newPanel.add(scroll);
        
        add(newPanel);
        
        pack();
        setLocationRelativeTo(null);
	}
	
	public String getPackageId() {
		return textPackageId.getText();
	}
	
	public JButton getSubmitButton() {
		return buttonGet;
	}
	
	public void setPackageStatus(String info) {
		textArea.setText(info);
	}

}
