import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class AdminBoard extends JFrame {
	private static final long serialVersionUID = 1L;
	private JButton buttonAddPackage = new JButton("Add new package");
	private JButton buttonRemovePackage = new JButton("Remove package");
	private JButton buttonRegisterForTracking = new JButton("Register package for tracking");
	private JButton buttonUpdatePackageStatus = new JButton("Update package status");
	
	public AdminBoard() {
		super("Admin operations board");
		
		JPanel newPanel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonAddPackage, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRemovePackage, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRegisterForTracking, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 4;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonUpdatePackageStatus, constraints);
        
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Admin operations board"));
        // add the panel to this frame
        add(newPanel);
        
        pack();
        setLocationRelativeTo(null);
	}
	
	public JButton getAddPackageButton() {
		return buttonAddPackage;
	}
	
	public JButton getRemovePackageButton() {
		return buttonRemovePackage;
	}
	
	public JButton getRegisterPackageButton() {
		return buttonRegisterForTracking;
	}
	
	public JButton getUpdatePackageButton() {
		return buttonUpdatePackageStatus;
	}

	@Override
	public String toString() {
		return "AdminBoard [buttonAddPackage=" + buttonAddPackage
				+ ", buttonRemovePackage=" + buttonRemovePackage
				+ ", buttonRegisterForTracking=" + buttonRegisterForTracking
				+ ", buttonUpdatePackageStatus=" + buttonUpdatePackageStatus
				+ "]";
	}
}
