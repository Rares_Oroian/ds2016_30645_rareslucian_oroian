package org.tempuri;

public class IServiceProxy implements org.tempuri.IService {
  private String _endpoint = null;
  private org.tempuri.IService iService = null;
  
  public IServiceProxy() {
    _initIServiceProxy();
  }
  
  public IServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIServiceProxy();
  }
  
  private void _initIServiceProxy() {
    try {
      iService = (new org.tempuri.ServiceLocator()).getBasicHttpBinding_IService();
      if (iService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iService != null)
      ((javax.xml.rpc.Stub)iService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.IService getIService() {
    if (iService == null)
      _initIServiceProxy();
    return iService;
  }
  
  public java.lang.String getData(java.lang.Integer value) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.getData(value);
  }
  
  public org.datacontract.schemas._2004._07.CompositeType getDataUsingDataContract(org.datacontract.schemas._2004._07.CompositeType composite) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.getDataUsingDataContract(composite);
  }
  
  public java.lang.String addPackage(java.lang.Integer sender, java.lang.Integer receiver, java.lang.String name, java.lang.String description, java.lang.String senderCity, java.lang.String destinationCity, java.lang.Boolean tracking) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.addPackage(sender, receiver, name, description, senderCity, destinationCity, tracking);
  }
  
  public java.lang.String removePackage(java.lang.Integer idPackage) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.removePackage(idPackage);
  }
  
  public java.lang.String updateStatus(java.lang.String city, java.lang.String time, java.lang.Integer idPackage) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.updateStatus(city, time, idPackage);
  }
  
  public java.lang.String registerForTracking(java.lang.Integer idPackage, java.lang.String city, java.lang.String time) throws java.rmi.RemoteException{
    if (iService == null)
      _initIServiceProxy();
    return iService.registerForTracking(idPackage, city, time);
  }
  
  
}