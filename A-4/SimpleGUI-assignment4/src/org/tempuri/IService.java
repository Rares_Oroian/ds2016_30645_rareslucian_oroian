/**
 * IService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IService extends java.rmi.Remote {
    public java.lang.String getData(java.lang.Integer value) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.CompositeType getDataUsingDataContract(org.datacontract.schemas._2004._07.CompositeType composite) throws java.rmi.RemoteException;
    public java.lang.String addPackage(java.lang.Integer sender, java.lang.Integer receiver, java.lang.String name, java.lang.String description, java.lang.String senderCity, java.lang.String destinationCity, java.lang.Boolean tracking) throws java.rmi.RemoteException;
    public java.lang.String removePackage(java.lang.Integer idPackage) throws java.rmi.RemoteException;
    public java.lang.String updateStatus(java.lang.String city, java.lang.String time, java.lang.Integer idPackage) throws java.rmi.RemoteException;
    public java.lang.String registerForTracking(java.lang.Integer idPackage, java.lang.String city, java.lang.String time) throws java.rmi.RemoteException;
}
