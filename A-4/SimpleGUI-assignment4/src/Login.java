import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
 


import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class Login extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelUsername = new JLabel("Enter username: ");
    private JLabel labelPassword = new JLabel("Enter password: ");
    private JLabel labelStatus = new JLabel("");
    private JTextField textUsername = new JTextField(20);
    private JPasswordField fieldPassword = new JPasswordField(20);
    private JButton buttonLogin = new JButton("Login");
    private JButton buttonRegister = new JButton("Register");
     
    public Login() {
        super("Login");
         
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        newPanel.add(labelUsername, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textUsername, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelPassword, constraints);
         
        constraints.gridx = 1;
        newPanel.add(fieldPassword, constraints);
         
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonLogin, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRegister, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(labelStatus, constraints);
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Login Panel"));
        // add the panel to this frame
        add(newPanel);
         
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        pack();
        setLocationRelativeTo(null);
    }

	public void setStatusText(String text, boolean success) {
		labelStatus.setText(text);
		
		if (success) {
			labelStatus.setForeground(Color.GREEN);
		} else {
			labelStatus.setForeground(Color.RED);
		}
	}

	public String getUsername() {
		return textUsername.getText();
	}

	public void setUsername(String text) {
		this.textUsername.setText(text);
	}

	@SuppressWarnings("deprecation")
	public String getPassword() {
		return fieldPassword.getText();
	}

	public void setFieldPassword(String password) {
		this.fieldPassword.setText(password);
	}

	public JButton getButtonLogin() {
		return buttonLogin;
	}

	public JButton getButtonRegister() {
		return buttonRegister;
	}
}
