import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import SimpleOperations.src.controller.SimpleOperationsStub.Package1;


public class AddPackage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel senderLabel = new JLabel("Sender ID: ");
    private JLabel receiverLabel = new JLabel("Receiver ID: ");
    private JLabel nameLabel = new JLabel("Package name:");
    private JLabel descriptionLabel = new JLabel("Package description:");
    private JLabel senderCityLabel = new JLabel("Sender city:");
    private JLabel destinationCityLabel = new JLabel("Destination city:");
    private JLabel labelStatus = new JLabel("");
    
    private JTextField senderText = new JTextField(10);
    private JTextField receiverText = new JTextField(10);
    private JTextField nameText = new JTextField(40);
    private JTextField descriptionText = new JTextField(40);
    private JTextField senderCityText = new JTextField(40);
    private JTextField destinationCityText = new JTextField(40);
    
    private JButton buttonCreate = new JButton("Create");
    
    public AddPackage() {
        super("Add package");
         
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 1;
        constraints.gridy = 1;     
        newPanel.add(senderLabel, constraints);
 
        constraints.gridx = 2;
        newPanel.add(senderText, constraints);
         
        constraints.gridx = 1;
        constraints.gridy = 2;     
        newPanel.add(receiverLabel, constraints);
         
        constraints.gridx = 2;
        newPanel.add(receiverText, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 3;     
        newPanel.add(nameLabel, constraints);
         
        constraints.gridx = 2;
        newPanel.add(nameText, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 4;     
        newPanel.add(descriptionLabel, constraints);
         
        constraints.gridx = 2;
        newPanel.add(descriptionText, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 5;     
        newPanel.add(senderCityLabel, constraints);
         
        constraints.gridx = 2;
        newPanel.add(senderCityText, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 6;     
        newPanel.add(destinationCityLabel, constraints);
         
        constraints.gridx = 2;
        newPanel.add(destinationCityText, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 7;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonCreate, constraints);
        
        constraints.gridx = 1;
        constraints.gridy = 8;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(labelStatus, constraints);
        
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Add a new package"));
        // add the panel to this frame
        add(newPanel);
        
        pack();
        
        setLocationRelativeTo(null);
    }
    
    public Package1 getPackage() {
    	Package1 p = new Package1();
    	
    	p.setSender(Integer.parseInt(senderText.getText()));
    	p.setReceiver(Integer.parseInt(receiverText.getText()));
    	p.setName(nameText.getText());
    	p.setDescription(descriptionText.getText());
    	p.setSenderCity(senderCityText.getText());
    	p.setDestinationCity(destinationCityText.getText());
    	
    	return p;
    }
    
    public JButton getSubmitButton() {
    	return buttonCreate;
    }
    
    public void setStatusText(String text, Boolean isGood) {
    	labelStatus.setText(text);
    	
    	if (isGood) {
    		labelStatus.setForeground(Color.GREEN);
    	} else {
    		labelStatus.setForeground(Color.RED);
    	}
    }
}