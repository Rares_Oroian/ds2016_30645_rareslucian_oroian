﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for PackageDAO
/// </summary>
public class PackageDAO
{
    public string addPackage(int sender, int receiver, string name, string description, string senderCity, string destinationCity, bool tracking)
    {
        string connString = "server=localhost;user id=root;password=root;database=assignmentfour";

        int track = 0;

        if (tracking)
            track = 1;

        MySqlConnection conn = new MySqlConnection(connString);
        conn.Open();
        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO package (sender, receiver, name, description, senderCity, destinationCity, track) " + 
               "VALUES (@sender, @receiver, @name, @description, @senderCity, @destinationCity, @track)";

        cmd.Prepare();

        cmd.Parameters.AddWithValue("@sender", sender);
        cmd.Parameters.AddWithValue("@receiver", receiver);
        cmd.Parameters.AddWithValue("@name", name);
        cmd.Parameters.AddWithValue("@description", description);
        cmd.Parameters.AddWithValue("@senderCity", senderCity);
        cmd.Parameters.AddWithValue("@destinationCity", destinationCity);
        cmd.Parameters.AddWithValue("@track", track);
        cmd.ExecuteNonQuery();

        conn.Close();

        return ("Successfully added row to table package!");
    }

    public string removePackage(int idPackage)
    {
        string connString = "server=localhost;user id=root;password=root;database=assignmentfour";

        MySqlConnection conn = new MySqlConnection(connString);
        conn.Open();
        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = conn;

        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "DELETE FROM package WHERE idPackage=" + idPackage;

        cmd.ExecuteNonQuery();
        conn.Close();

        return ("Successfully deleted the package!");
    }

    // a new entry (City, Time) is introduced to the route
    public string updateStatus(string city, string time, int idPackage)
    {
        string connString = "server=localhost;user id=root;password=root;database=assignmentfour";

        MySqlConnection conn = new MySqlConnection(connString);
        conn.Open();
        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = conn;

        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT INTO route (city, time, package) VALUES (@city, @time, @package)";

        cmd.Prepare();

        cmd.Parameters.AddWithValue("@city", city);
        cmd.Parameters.AddWithValue("@time", time);
        cmd.Parameters.AddWithValue("@package", idPackage);

        cmd.ExecuteNonQuery();
        conn.Close();

        return ("Successfully added a row in route table!");
    }

    public string registerForTracking(int idPackage, string city, string time)
    {

        string connString = "server=localhost;user id=root;password=root;database=assignmentfour";

        MySqlConnection conn = new MySqlConnection(connString);
        conn.Open();
        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE package SET track = 1 WHERE idPackage=" + idPackage;
        cmd.Parameters.AddWithValue("@id", idPackage);

        cmd.ExecuteNonQuery();
        conn.Close();

        updateStatus(city, time, idPackage);

        return ("Successfully registered package!");
    }
}