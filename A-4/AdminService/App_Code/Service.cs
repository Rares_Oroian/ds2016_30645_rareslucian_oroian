﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
    public string GetData(int value)
    {
        return string.Format("You entered: {0}", value);
    }

    public string addPackage(int sender, int receiver, string name, string description, string senderCity, string destinationCity, bool tracking)
    {
        PackageDAO packDAO = new PackageDAO();

        return packDAO.addPackage(sender, receiver, name, description, senderCity, destinationCity, tracking);
    }


    public string removePackage(int idPackage)
    {
        PackageDAO packDAO = new PackageDAO();

        return packDAO.removePackage(idPackage);
    }


    public string updateStatus(string city, string time, int package)
    {
        PackageDAO packDAO = new PackageDAO();

        return packDAO.updateStatus(city, time, package);
    }


    public string registerForTracking(int idPackage, string city, string time)
    {
        PackageDAO packDAO = new PackageDAO();

        return packDAO.registerForTracking(idPackage, city, time);
    }



    public CompositeType GetDataUsingDataContract(CompositeType composite)
    {
        if (composite == null)
        {
            throw new ArgumentNullException("composite");
        }
        if (composite.BoolValue)
        {
            composite.StringValue += "Suffix";
        }
        return composite;
    }
}
