package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import assignment.three.common.classes.Dvd;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

public class FilesWriterClient {
	
	public FilesWriterClient() {
	}
	
	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);
		Dvd message;
		
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream("/messages-log.txt");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		int cnt = 0;
		while(true) {
			try {
				message = queue.readMessage();
				cnt++;
				System.out.println("Received " + message.toString());
				
				if (cnt < 5) {
					fileOut.write(message.toString().getBytes());
				} else {
					fileOut.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
