package store.employee; /**
 * Created by Rares on 12/10/2016.
 */

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import common.Dvd;

public class Publisher {
    private static final String EXCHANGE_NAME = "dvds-exchange";

    public static void publishDvd(Dvd dvd) {
        try {
            publishToExchange(dvd.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void publishToExchange(String stringMessage) throws java.io.IOException, java.util.concurrent.TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        channel.basicPublish(EXCHANGE_NAME, "", null, stringMessage.getBytes());
        System.out.println(" [x] Sent '" + stringMessage + "'");

        channel.close();
        connection.close();
    }
}
