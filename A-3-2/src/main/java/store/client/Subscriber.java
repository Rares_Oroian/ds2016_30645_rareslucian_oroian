package store.client; /**
 * Created by Rares on 12/10/2016.
 */
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Date;

public class Subscriber {
    private static final String EXCHANGE_NAME = "dvds-exchange";
    private static final String QUEUE_NAME = "dvd-store-client-1";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");

        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                Date now = new Date();
                String message = now.toString() + ": " + new String(body, "UTF-8") + "\n";
                System.out.println(" [x] Received '" + message + "'");

                // write to file
                FileWriterUtility fileWriter = new FileWriterUtility("C:\\Users\\Rares\\IdeaProjects\\Assignment-3.2\\src\\main\\java\\store\\client\\dvds.txt");
                fileWriter.append(message);

                // send email
                MailService mailService = new MailService("rares94test@gmail.com","rares94test1234");
                mailService.sendMail("oroian.rares@gmail.com", "New DVD notification", message);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
