package store.client;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class FileWriterUtility {
    private String FILENAME;

    public FileWriterUtility(String filename) {
        FILENAME = filename;
    }

    public void append(String textContent) throws IOException {
        System.out.println("Writing to file: " + FILENAME);

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(FILENAME, true);
            bw = new BufferedWriter(fw);

            bw.write(textContent);

            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
