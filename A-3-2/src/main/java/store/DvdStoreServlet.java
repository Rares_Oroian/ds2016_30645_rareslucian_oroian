package store;

import common.Dvd;
import store.employee.Publisher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DvdStoreServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Dvd dvd = new Dvd(
                request.getParameter("dvdTitle"),
                Integer.parseInt(request.getParameter("dvdYear")),
                Double.parseDouble(request.getParameter("dvdPrice"))
        );

        Publisher.publishDvd(dvd);

        System.out.println("POST called!" + dvd.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("GET called!");

        request.setAttribute("title", "DVD Store App");
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
