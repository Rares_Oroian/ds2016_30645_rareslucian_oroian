<%--
  Created by IntelliJ IDEA.
  User: Rares
  Date: 12/10/2016
  Time: 6:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% String title = (String)request.getAttribute("title"); %>

<html>
  <head>
    <title><%= title%></title>
  </head>
  <body>
    <h3>Hello!</h3>

    <form action="/store" method="POST">
        <span>Title:</span><input type="text" name="dvdTitle" placeholder="DVD Title"><br>
        <span>Year:</span><input type="number" name="dvdYear" placeholder="DVD Year"><br>
        <span>Price:</span><input type="number" name="dvdPrice" placeholder="DVD Price">
        <hr>
        <input type="submit" value="Submit form "/>
    </form>
  </body>
</html>
