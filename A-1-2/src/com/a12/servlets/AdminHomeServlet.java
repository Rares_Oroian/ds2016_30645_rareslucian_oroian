package com.a12.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import com.a12.dao.FlightDAO;
import com.a12.entities.City;
import com.a12.entities.Flight;
import com.a12.entities.User;
import com.a12.utility.HtmlBuilder;

/**
 * Servlet implementation class AdminHome
 */
@WebServlet("/AdminHomeServlet")
public class AdminHomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HtmlBuilder htmlBuilder = null;
	private FlightDAO flightDao = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminHomeServlet() {
        super();
        htmlBuilder = new HtmlBuilder();
        flightDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		
		if (user != null) {
			if (isAuthorized(user)) {
				RequestDispatcher view = request.getRequestDispatcher("/admin-home.html");
				view.forward(request, response);
			} else {
				response.sendRedirect("forbidden-access");
			}
		} else {
			response.sendRedirect("login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String formOperation = request.getParameter("operation");
		
		if (formOperation != null) {
			request.setAttribute("operation", null);
			
			Flight oldFlight = (Flight) request.getSession().getAttribute("flight");
			System.out.println(formOperation + oldFlight.toString());
			if (formOperation.equals("delete")) {
				flightDao.deleteFlight(oldFlight);
			} else  {
				Flight flight = createFlightFromFormData(request);
				
				if (flight != null) {
					if (formOperation.equals("update")) {
						flight.setId(oldFlight.getId());
						flightDao.saveOrUpdateFlight(flight);
					}
					
					if (formOperation.equals("create")) {
						flightDao.saveOrUpdateFlight(flight);
					}
				}
			}
			
			doGet(request, response);
			
			return;
		}
		
		String form = "";
		String option = request.getParameter("option");
		
		if (option.equals("Create Flight")) {
			form = htmlBuilder.generateFlightForm("Flight form to fill", null, "create", true);
		}
		if (option.equals("Read Flight") || option.equals("Update Flight") || option.equals("Delete Flight")) {
			Flight flight = flightDao.getFlightWithId(Integer.parseInt(request.getParameter("flightId")));
			request.getSession().setAttribute("flight", flight);
			
			if (option.equals("Read Flight")) {
				form = htmlBuilder.generateFlightForm("Flight data read", flight, "read", false);
			}
			if (option.equals("Update Flight")) {
				form = htmlBuilder.generateFlightForm("Flight data tu update", flight, "update", true);
			}
			if (option.equals("Delete Flight")) {
				form = htmlBuilder.generateFlightForm("Deleted flight", flight, "delete", true);
			}
		}
		
		PrintWriter out = response.getWriter();
		out.println(htmlBuilder.generateDoc(
				htmlBuilder.generateHead("Flight"),
				htmlBuilder.generateBody(form)
		));
	}

	private boolean isAuthorized(User user) {
		if (user != null) {
			return user.getType().equals("admin");
		}
		
		return false;
	}
	
	private Flight createFlightFromFormData(HttpServletRequest request) {
		Flight flight = null;
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			
			flight = new Flight(
				(int)Float.parseFloat(request.getParameter("flightNumber")),
				request.getParameter("flightAirplaneType"),
				formatter.parse(request.getParameter("flightDepartureTime")),
				formatter.parse(request.getParameter("flightArrivalTime")),
				new City(
					request.getParameter("departureCityName"),
					Float.parseFloat(request.getParameter("departureCityLatitude")),
					Float.parseFloat(request.getParameter("departureCityLongitude"))
				),
				new City(
					request.getParameter("arrivalCityName"),
					Float.parseFloat(request.getParameter("arrivalCityLatitude")),
					Float.parseFloat(request.getParameter("arrivalCityLongitude"))
				));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return flight;
	}
}
