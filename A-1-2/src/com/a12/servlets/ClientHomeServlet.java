package com.a12.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import com.a12.dao.UserFlightDAO;
import com.a12.entities.Flight;
import com.a12.entities.User;
import com.a12.entities.UserFlight;
import com.a12.utility.HtmlBuilder;

/**
 * Servlet implementation class ClientHomeServlet
 */
@WebServlet("/ClientHomeServlet")
public class ClientHomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HtmlBuilder htmlBuilder = null;
	private UserFlightDAO userFlightDao = null;
	private User user = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientHomeServlet() {
        super();
        this.htmlBuilder = new HtmlBuilder();
        this.userFlightDao = new UserFlightDAO(new Configuration().configure().buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.user = (User) request.getSession().getAttribute("user");
		
		if (user != null) {
			if (isAuthorized(user)) {
				PrintWriter out = response.getWriter();
				out.println(generatePageContent());
			} else {
				response.sendRedirect("forbidden-access");
			}
		} else {
			response.sendRedirect("login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private boolean isAuthorized(User user) {
		if (user != null) {
			return user.getType().equals("client");
		}
		
		return false;
	}
	
	private String generatePageContent() {
		String head = htmlBuilder.generateHead("Client home page");
		List<UserFlight> userFlights = userFlightDao.getUserFlightsList(user.getId());
		
		System.out.println(userFlights.toString());
		
		String tableHead = "<tr>" + 
				"<th>Flight id</th>" +
				"<th>Flight number</th>" +
				"<th>Airplane type</th>" +
				"<th>Departure city</th>" +
				"<th>Departure time</th>" +
				"<th>Arrival city</th>" +
				"<th>Arrival time</th>" +
				"</tr>";
		
		String tableBody = "";
		for (UserFlight userFlight: userFlights) {
			Flight flight = userFlight.getFlight();
			tableBody += 
				"<tr>" +
					"<td>" + flight.getId() + "</td>" +
					"<td>" + flight.getFlightNumber() + "</td>" +
					"<td>" + flight.getAirplaneType() + "</td>" +
					"<td>" + flight.getDepartureCity().getName() + "</td>" +
					"<td>" + flight.getDepartureTime().toString() + "</td>" +
					"<td>" + flight.getArrivalCity().getName() + "</td>" +
					"<td>" + flight.getArrivalTime().toString() + "</td>" +
				"</tr>";
		}

		String body = htmlBuilder.generateBody("<h2>Your flights</h2><table>" + tableHead + tableBody + "</table>");
		
		return htmlBuilder.generateDoc(head, body);
	}
	
//	private String getLocalTime(float latitude, float longitude) {
//		try {
//			URL yahoo;
//			yahoo = new URL("http://www.earthtools.org/timezone/" + latitude + "/" + longitude);
//			URLConnection yc = yahoo.openConnection();
//	        BufferedReader in = new BufferedReader(
//	                                new InputStreamReader(
//	                                yc.getInputStream()));
//	        String inputLine;
//
//	        while ((inputLine = in.readLine()) != null) 
//	            System.out.println(inputLine);
//	        
//	        in.close();
//	        
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        
//        return "test";
//	}
}
