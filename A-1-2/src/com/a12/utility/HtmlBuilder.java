package com.a12.utility;

import java.util.Date;

import com.a12.entities.City;
import com.a12.entities.Flight;

public class HtmlBuilder {
	public HtmlBuilder() {}
	
	public String generateDoc(String head, String body) {
		return 
			"<!DOCTYPE html>" +
			"<html>" +
				head +
				body +
			"</html>";
	}
	
	public String generateHead(String title) {
		return 
			"<head>" +
				"<meta charset=\"ISO-8859-1\">" +
				"<title>" + title + "</title>" +
			"</head>";
	}
	
	public String generateBody(String bodyContent) {
		return "<body>" + bodyContent + "</body>";
	}
	
	public String generateSimpleLabel(String label) {
		return "<label>" + label + "</label>";
	}
	
	
	public String generateInput(String type, String name, String value, boolean disabled) {
		if (disabled) {
			return "<input type=\"" + type + "\" name=\"" + name + "\" value=\"" + value + "\" disabled />";
		} else {
			return "<input type=\"" + type + "\" name=\"" + name + "\" value=\"" + value + "\" />";
		}
	}
	
	public String generateInput(String type, String name, float value, boolean disabled) {
		return generateInput(type, name, Float.toString(value), disabled);
	}
	
	public String generateInput(String type, String name, Date value, boolean disabled) {
		return generateInput(type, name, "" + value, disabled);
	}
	
	public String generateCityFormInputs(String prefix, City city, boolean disabled) {
		return generateSimpleLabel(" Name: ") +
			generateInput("text", prefix + "CityName", city.getName(), disabled) +
			generateSimpleLabel(" Latitude: ") +
			generateInput("text", prefix + "CityLatitude", city.getLatitude(), disabled) +
			generateSimpleLabel(" Longitude: ") +
			generateInput("text", prefix + "CityLongitude", city.getLongitude(), disabled);
	}
	
	public String generateFlightForm(String formName, Flight flight, String operation, boolean hasSubmit) {
		boolean disabled = false;
		
		if (flight == null) {
			flight = new Flight();
		}
		
		if (operation.equals("read") || operation.equals("delete")) {
			disabled = true;
		}
		
		return 
			"<form method=\"post\">" +
				"<h2>" + formName + "</h2><br>" +
				"Flight departure city:<br>" +
				generateCityFormInputs("departure", flight.getDepartureCity(), disabled) + 
				"<br>Flight arrival city:<br>" +
				generateCityFormInputs("arrival", flight.getArrivalCity(), disabled) +
				"<br><br>Flight details:<br>" +
				generateSimpleLabel("Flight number: ") +
				generateInput("number", "flightNumber", flight.getFlightNumber(), disabled) + "<br>" +
				generateSimpleLabel(" Airplane type: ") +
				generateInput("text", "flightAirplaneType", flight.getAirplaneType(), disabled) + "<br>" +
				generateSimpleLabel(" Departure time: ") +
				generateInput("text", "flightDepartureTime", flight.getDepartureTime(), disabled) + "<br>" +
				generateSimpleLabel(" Arrival time: ") +
				generateInput("text", "flightArrivalTime", flight.getArrivalTime(), disabled) +
				"<br><br>" +
				((hasSubmit) ? generateInput("submit", "operation", operation, false) : "") +
			"</form>";
	}
}
