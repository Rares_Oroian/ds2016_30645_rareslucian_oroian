package com.a12.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.a12.entities.Flight;

public class FlightDAO {
	private SessionFactory factory;
	
	public FlightDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	public Flight getFlightWithId(int flightId) {
		Session session = factory.openSession();
		Transaction tx = null;
		Flight flight = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.createQuery("FROM Flight WHERE idFlight=" + flightId);
			flight = (Flight) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return flight;
	}
	
	public Flight saveOrUpdateFlight(Flight flight) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			session.saveOrUpdate(flight);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return null;
	}
	
	public boolean deleteFlight(Flight flight) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			session.delete(flight);
			tx.commit();
			
			return true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return false;
	}
}
