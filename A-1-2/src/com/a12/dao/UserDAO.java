package com.a12.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.a12.entities.User;

public class UserDAO {
	private SessionFactory factory;
	
	public UserDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	public User authenticateUser(User credentials) {
		User user = getUserByUsername(credentials.getUsername());
		
		if (user != null) {
			if (credentials.getPassword().equals(user.getPassword())) {
				return user;
			}
		}
		
		return null;
	}
	
	private User getUserByUsername(String username) {
		Session session = factory.openSession();
		Transaction tx = null;
		User user = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.createQuery("FROM User WHERE username='" + username + "'");
			user = (User) query.uniqueResult();
			tx.commit();
		} catch(Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return user;
	}
}
