package com.a12.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.a12.entities.City;

public class CityDAO {
private SessionFactory factory;
	
	public CityDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	public City getCityWithName(String cityName) {
		Session session = factory.openSession();
		Transaction tx = null;
		City city = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.createQuery("FROM City WHERE name=" + cityName);
			city = (City) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return city;
	}
	
	public City saveOrUpdateCity(City city) {
		Session session = factory.openSession();
		Transaction tx = null;
		City found = getCityWithName(city.getName());
		
		if (city.getId() == 0 && found != null) {
			city.setId(found.getId());
		}
		
		try {
			tx = session.getTransaction();
			tx.begin();
			session.saveOrUpdate(city);
			tx.commit();
			
			return getCityWithName(city.getName());
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return found;
	}
}
