package com.a12.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.a12.entities.UserFlight;

public class UserFlightDAO {
	private SessionFactory factory;
	
	public UserFlightDAO(SessionFactory factory) {
		this.factory = factory;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserFlight> getUserFlightsList(int userId) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<UserFlight> userFlightsList = null;
		
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.createQuery("FROM UserFlight WHERE user.id=" + userId);
			userFlightsList = query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return userFlightsList;
	}
}
