package com.a12.entities;

import java.util.Date;

public class Flight {
	private int id;
	private int flightNumber;
	private String airplaneType;
	private Date departureTime;
	private Date arrivalTime;
	
	private City departureCity;
	private City arrivalCity;
	
	public Flight() {
		departureCity = new City();
		arrivalCity = new City();
	}
	
	public Flight(int flightNumber, String airplaneType, Date departureTime, Date arrivalTime, City departureCity, City arrivalCity) {
		this.flightNumber = flightNumber;
		this.airplaneType = airplaneType;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public int getFlightNumber() {
		return flightNumber;
	}
	
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	
	public String getAirplaneType() {
		return airplaneType;
	}
	
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public Date getArrivalTime() {
		return arrivalTime;
	}
	
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	
	public Date getDepartureTime() {
		return departureTime;
	}
	
	public City getDepartureCity() {
		return departureCity;
	}
	
	public void setDepartureCity(City city) {
		this.departureCity = city;
	}
	
	public City getArrivalCity() {
		return arrivalCity;
	}
	
	public void setArrivalCity(City city) {
		this.arrivalCity = city;
	}
	
	@Override
	public String toString() {
		return "Flight(id: " + id + "; flightNumber: " + flightNumber + "; airplateType: " + airplaneType +
				"; departureCity: " + departureCity.toString() + 
				"; arrivalCity: " + arrivalCity.toString() + 
				"; departureTime: " + departureTime + "; arrivalTime: " + arrivalTime + ");";
	}
}
