package com.a12.entities;

public class UserFlight {
	private int id;
	
	private User user;
	private Flight flight;
	
	public UserFlight() {}
	public UserFlight(User user, Flight flight) {
		this.user = user;
		this.flight = flight;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public Flight getFlight() {
		return flight;
	}
	
	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	
	@Override
	public String toString() {
		return "UserFlight (id: " + id + "; user: " + user.toString() + "; flight: " + flight.toString() + ");";
	}
}
