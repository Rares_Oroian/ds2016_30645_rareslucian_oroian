(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('UpdateBookCtrl', updateBookCtrlFunc);

  updateBookCtrlFunc.$inject = ['$scope', 'book', 'bookIdentifier'];

  function updateBookCtrlFunc($scope, book, bookId) {
    book.get(bookId).then(function(bookData) {
      $scope.name = bookData.name;
      $scope.author = bookData.author;
      $scope.availableStock = bookData.availableStock;
      $scope.totalStock = bookData.totalStock;
    }, function(err) {
      $scope.errorMsg = 'Could fetch book data!';
    });

    $scope.update = function(newBookData) {
      if(!newBookData.name || !newBookData.author || isNaN(newBookData.availableStock) || isNaN(newBookData.totalStock)) {
        return $scope.errorMsg = 'Invalid new data!';
      }

      newBookData.id = bookId;

      book.update(newBookData).then(function(resp) {
        $scope.successMsg = 'Update successful!';
      }, function(err) {
        $scope.errorMsg = 'Update failed!';
      });
    };
  }
}());
