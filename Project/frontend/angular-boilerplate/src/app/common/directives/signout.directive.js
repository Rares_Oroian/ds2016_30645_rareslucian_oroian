(function() {
  angular
  .module('website.common')
  .directive('signoutControl', signoutControlFunc);

  signoutControlFunc.$inject = ['$state', 'auth', 'storage'];

  function signoutControlFunc($state, auth, storage) {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'app/common/directives/signout.template.html',
      link: function(scope) {
        scope.signOut = function() {
          auth
            .signOut(storage.loadSecrets())
            .then(actionHandler, actionHandler);
        };

        function actionHandler() {
          $state.go('login');
          storage.deleteSecrets();
        }
      }
    }
  }
})();
