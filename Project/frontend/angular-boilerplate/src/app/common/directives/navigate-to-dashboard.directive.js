(function() {
  'use strict';
  angular
  .module('website.common')
  .directive('navigateToDashboard', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/common/directives/navigate-to-dashboard.html'
    };
  });
}());
