(function() {
  angular
  .module('website.common')
  .service('storage', storageFunc);

  storageFunc.$inject = [];

  function storageFunc() {
    var USER_SECRETS_KEY = 'sdasdsafgr34wge';
    var exposedAPI = {
      storeSecrets: storeSecrets,
      loadSecrets: loadSecrets,
      deleteSecrets: deleteSecrets
    };

    function storeSecrets(secrets) {
      var secretsString = '';

      try {
        secretsString = JSON.stringify(secrets);
      } catch (err) {
        console.log('Error on JSON.stringify of secrets object', err);
      }

      // save to local storage
      localStorage[USER_SECRETS_KEY] = secretsString;
    }

    function loadSecrets() {
      var secretsString = localStorage[USER_SECRETS_KEY];
      var secrets = {};

      try {
        secrets = JSON.parse(secretsString);
      } catch (err) {
        console.log('Error on JSON.parse of secrets string.', err);
      }

      return secrets;
    }

    function deleteSecrets() {
      localStorage[USER_SECRETS_KEY] = {};
    }

    return exposedAPI;
  }
})();
