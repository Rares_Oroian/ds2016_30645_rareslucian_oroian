(function() {
  'use strict';

  angular
  .module('website.manager-dashboard')
  .controller('SendNotificationCtrl', sendNotificationCtrlFunc);

  sendNotificationCtrlFunc.$inject = [
    '$scope',
    'userId',
    'user',
    'notification'
  ];

  function sendNotificationCtrlFunc($scope, userId, user, notification) {
    user.get(userId).then(function(resp) {
      $scope.userData = resp;
    }, function(err) {
      $scope.errorMsg = 'Failed to fetch user data!';
    });

    $scope.submit = function(subject, body) {
      $scope.errorMsg = '';
      $scope.successMsg = '';

      if (!subject || !body) {
        return $scope.errorMsg = 'Missing title or body!';
      }

      var mail = {
        subject: subject,
        body: body
      };

      notification.send(userId, mail).then(function(resp) {
        $scope.successMsg = 'Message sent!';
      }, function(err) {
        $scope.errorMsg = 'Message not sent!';
      });
    };
  }
}());
