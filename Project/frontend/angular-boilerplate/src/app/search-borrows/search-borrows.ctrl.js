(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('SearchBorrowsCtrl', searchBorrowsCtrlFunc);

  searchBorrowsCtrlFunc.$inject = ['$scope', 'borrow'];

  function searchBorrowsCtrlFunc($scope, borrow) {
    $scope.borrowsArr = [];
    $scope.visibleBorrowsArr = [];

    $scope.findAll = function() {
      borrow.findAll().then(function(borrowsArr) {
        $scope.borrowsArr = borrowsArr;
        $scope.visibleBorrowsArr = borrowsArr;
      }, function(err) {
        $scope.errorMsg = 'Failed to get all borrows list!';
      });
    };

    $scope.find = function(substring) {
      if (!substring) {
        return $scope.visibleBorrowsArr = $scope.borrowsArr;
      }

      var selectedArr = [];

      for (var i = 0; i < $scope.borrowsArr.length; i++) {
        var borrow = $scope.borrowsArr[i];

        if (borrow.username.indexOf(substring) >= 0 || borrow.bookName.indexOf(substring) >= 0) {
          selectedArr.push(borrow);
        }
      }

      $scope.visibleBorrowsArr = selectedArr;
    }

    $scope.returnBorrow = function(borrowId) {
      if (!borrowId) return;

      borrow.returnBorrow(borrowId).then(function() {
        $scope.findAll();
      });
    }

    $scope.findAll();
  }
}());
