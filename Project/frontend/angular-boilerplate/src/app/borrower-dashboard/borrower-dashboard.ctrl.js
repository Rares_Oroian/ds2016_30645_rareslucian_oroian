(function() {
  'use strict';
  angular
  .module('website.borrower-dashboard')
  .controller('BorrowerDashboardCtrl', borrowerDashboardCtrlFunc);

  borrowerDashboardCtrlFunc.$inject = ['$scope', 'borrow', 'storage'];

  function borrowerDashboardCtrlFunc($scope, borrow, storage) {
    var userId = storage.loadSecrets().userId;

    borrow.findForUser(userId).then(function(data) {
      $scope.dataArr = data;
    }, function(err) {
      alert('Failed to get you borrows data!');
    });
  }
}());
