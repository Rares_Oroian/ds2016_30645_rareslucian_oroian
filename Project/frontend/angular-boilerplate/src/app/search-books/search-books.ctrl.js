(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('SearchBooksCtrl', searchBooksCtrlFunc);

  searchBooksCtrlFunc.$inject = ['$state', '$scope', 'book'];

  function searchBooksCtrlFunc($state, $scope, book) {
    $scope.booksArr = [];
    $scope.visibleBooks = [];

    $scope.findAll = function() {
      book.getAll().then(function(booksArr) {
        $scope.booksArr = booksArr;
        $scope.visibleBooks = booksArr;
      });
    };

    $scope.find = function(substring) {
      var selectedBooks = [];

      if (!substring) {
        return $scope.visibleBooks = $scope.booksArr;
      }

      for (var i = 0; i < $scope.booksArr.length; i++) {
        var book = $scope.booksArr[i];

        if (book.name.indexOf(substring) >= 0) {
          selectedBooks.push(book);
        }
      }

      $scope.visibleBooks = selectedBooks;
    };

    $scope.updateBook = function(id) {
      $state.go('update-book', {bookId: id});
    };

    $scope.deleteBook = function(id) {
      book.delete(id).then(function(resp) {
        $scope.findAll();
      }, function(err) {
        alert('Delete failed!');
      });
    }

    $scope.findAll();
  }
}());
