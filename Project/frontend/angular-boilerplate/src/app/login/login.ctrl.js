(function() {
    angular
    .module('website.login')
    .controller('LoginCtrl', loginCtrlFunc);

    loginCtrlFunc.$inject = ['$scope', '$state', 'auth', 'storage'];

    function loginCtrlFunc($scope, $state, auth, storage) {

      $scope.submitCredentials = function(credentials) {
        $scope.errorMessage = null;

        if (!credentials.username || !credentials.password) {
          return $scope.errorMessage = 'Incomplete credentials!';
        }

        auth
          .signIn(credentials)
          .then(function(resp) {
            resp.secret.isManager = resp.isManager;
            storage.storeSecrets(resp.secret);

            if(resp.isManager) {
              $state.go('manager-dashboard');
            } else {
              $state.go('borrower-dashboard');
            }
          }, function(err) {
            if (err.status === 401) {
              $scope.errorMessage = 'Invalid credentials!';
            } else {
              $scope.errorMessage = 'Unexpected server error!'
            }
          });
      }
    }
}());
