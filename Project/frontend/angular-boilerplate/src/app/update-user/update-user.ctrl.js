(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('UpdateUserCtrl', updateUserCtrlFunc);

  updateUserCtrlFunc.$inject = ['$scope', 'userId', 'user'];

   function updateUserCtrlFunc($scope, userId, user) {
     user.get(userId).then(function(user) {
       $scope.userData = user;
     }, function(err) {
       $scope.errorMsg = 'Failed to fetch user data!';
     });

     $scope.update = function(userData) {
       $scope.successMsg = '';
       $scope.errorMsg = '';
       
       user.update(userId, userData).then(function(resp) {
         $scope.successMsg = 'Update successful!';
       }, function(err) {
         $scope.errorMsg = 'Update failed!';
       });
     }
   }
}());
