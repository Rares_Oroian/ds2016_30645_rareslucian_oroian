(function() {

  angular
  .module('website.manager-dashboard')
  .controller('CreateBookCtrl', createBookCtrlFunc);

  createBookCtrlFunc.$inject = ['$scope', 'book'];

  function createBookCtrlFunc($scope, book) {

    $scope.createBook = function(bookObj) {
      $scope.errorMsg = '';
      $scope.successMsg = '';

      if (!bookObj.name || !bookObj.author || !bookObj.pieces) {
        $scope.errorMsg = 'Invalid book input';
      }

      book.create(bookObj).then(function(resp) {
        $scope.successMsg = 'Book created successfully!';
      }, function(err) {
        console.log('book creation failed', err);
        $scope.errorMsg = 'Unexpected error occured!';
      });
    }
  }
})();
