(function() {
  angular
  .module('website.manager-dashboard')
  .controller('ManagerDashboardCtrl', managerDashboardCtrlFunc);

  managerDashboardCtrlFunc.$inject = ['$scope', '$q', 'borrow', 'book'];

  function managerDashboardCtrlFunc($scope, $q, borrow, book) {
    $scope.chart = {
        type: 'pie',
        labels: ['In stock', 'Borrowed'],
        data: [],
        options: {}
    };

    book.totalStock().then(function(totalStock) {
      $scope.totalStock = totalStock;

      return borrow.getTotal();
    }, function(err) {
      alert('Failed to get total stock!');
    }).then(function(totalBorrowed) {
      $scope.totalBorrowed = totalBorrowed;

      return $q.when([$scope.totalStock - totalBorrowed, $scope.totalBorrowed]);
    }, function(err) {
      alert('Failed to get total borrowed!');
    }).then(function(arr) {
      $scope.chart.data = arr;
    });
  }
})();
