(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('LendBookCtrl', lendBookCtrlFunc);

  lendBookCtrlFunc.$inject = ['$scope', 'user', 'book', 'borrow'];

  function lendBookCtrlFunc($scope, user, book, borrow) {
    $scope.usersArr = [];
    $scope.booksArr = [];
    $scope.days = 14;
    $scope.quantity = 1;

    function init() {
      $scope.username = '';
      $scope.bookname = '';
      $scope.maxQuantity = '';

      // get all users
      user.findAll().then(function(arr) {
        $scope.usersArr = arr;
      }, function(err) {
        alert('Failed to get users!');
      });
      // get all books
      book.getAll().then(function(arr) {
        $scope.booksArr = arr;
      }, function(err) {
        alert('Failed to get books!');
      });
    }

    init();

    $scope.borrow = function(username, bookname, days, quantity) {
      $scope.errorMsg = '';
      $scope.successMsg = '';
      var tokens;

      tokens = username.split('.');
      var userId = parseInt(tokens[0]);

      tokens = bookname.split('.');
      var bookId = parseInt(tokens[0]);

      days = days || $scope.days;
      quantity = quantity || 1;

      borrow.borrow(bookId, userId, days, quantity).then(function(resp) {
        $scope.successMsg = 'Book borrow registration successful!';
        init();
      }, function(err) {
        $scope.errorMsg = 'Borrow action failed!';
      });
    };

    $scope.$watch('bookname', function() {
      if (!$scope.bookname) return;

      var tokens = $scope.bookname.split('.');
      var bookId = parseInt(tokens[0]);

      if (bookId) {
        for (var i = 0; i < $scope.booksArr.length; i++) {
          var book = $scope.booksArr[i];

          if (book.id == bookId) {
            return $scope.maxQuantity = book.availableStock;
          }
        }
      }
    });
  }
}());
