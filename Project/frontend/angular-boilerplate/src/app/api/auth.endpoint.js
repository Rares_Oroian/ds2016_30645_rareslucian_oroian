(function() {
  angular
  .module('website.api')
  .factory('auth', authEndpointFunc);

  authEndpointFunc.$inject = [
    '$http',
    '$q',
    'storage',
    'SERVER_DOMAIN',
    'LOGIN_ENDPOINT',
    'LOGOUT_ENDPOINT'
  ];

  function authEndpointFunc(
    $http,
    $q,
    storage,
    SERVER_DOMAIN,
    LOGIN_ENDPOINT,
    LOGOUT_ENDPOINT
  ) {
    var exposedAPI = {
      signIn: signIn,
      signOut: signOut,
      authoriseOpUrl: enhanceUrlWithSecret
    };

    function signIn(credentials) {
      var postObj = {
        username: credentials.username,
        password: credentials.password
      };

      return $http
        .post(SERVER_DOMAIN + LOGIN_ENDPOINT, postObj)
        .then(function(resp) {
          return $q.when(resp.data);
        }, function(err) {
          return $q.reject(err);
        });
    }

    function signOut(secrets) {
      var postObj = {
        userId: secrets.userId,
        secret: secrets.secret
      };

      return $http.post(SERVER_DOMAIN + LOGOUT_ENDPOINT, postObj);
    }

    function enhanceUrlWithSecret(url, hasParams) {
      var secrets = storage.loadSecrets();

      if (hasParams) {
        url += '&';
      } else {
        url += '?';
      }

      url += 'key=' + secrets.secret + '&' + 'value=' + secrets.userId;

      return url;
    }

    return exposedAPI;
  }

})();
