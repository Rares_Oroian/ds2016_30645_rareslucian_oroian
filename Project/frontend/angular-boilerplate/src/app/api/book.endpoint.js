(function() {
  'use strict';

  angular
  .module('website.manager-dashboard')
  .factory('book', bookFunc);

  bookFunc.$inject = [
    '$q',
    '$http',
    'auth',
    'SERVER_DOMAIN',
    'CREATE_BOOK',
    'GET_BOOK',
    'GET_ALL_BOOKS',
    'TOTAL_STOCK'
  ];

  function bookFunc(
    $q,
    $http,
    auth,
    SERVER_DOMAIN,
    CREATE_BOOK,
    GET_BOOK,
    GET_ALL_BOOKS,
    TOTAL_STOCK
  ) {
    var exposedAPI = {
      create: createBook,
      get: getBook,
      getAll: getAllBooks,
      update: updateBook,
      delete: deleteBook,
      totalStock: getTotalStock
    };

    function createBook(bookObj) {
      var postObj = {
        name: bookObj.name,
        author: bookObj.author,
        availableStock: bookObj.pieces,
        totalStock: bookObj.pieces
      };

      return $http.post(SERVER_DOMAIN + CREATE_BOOK, postObj);
    }

    function updateBook(newBookData) {
      var updateObj = {
        id: newBookData.id,
        name: newBookData.name,
        author: newBookData.author,
        availableStock: newBookData.availableStock || 0,
        totalStock: newBookData.totalStock || 0
      };
      var url = SERVER_DOMAIN + GET_BOOK + updateObj.id;

      return $http.put(auth.authoriseOpUrl(url), updateObj);
    }

    function deleteBook(bookId) {
      var url = SERVER_DOMAIN + GET_BOOK + bookId;

      return $http.delete(auth.authoriseOpUrl(url));
    }

    function getBook(bookId) {
      var url = SERVER_DOMAIN + GET_BOOK + bookId;

      return $http.get(url).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    function getAllBooks() {
      var url = SERVER_DOMAIN + GET_ALL_BOOKS;

      return $http.get(url).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    function getTotalStock() {
      var url = SERVER_DOMAIN + TOTAL_STOCK;

      return $http.get(auth.authoriseOpUrl(url)).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    return exposedAPI;
  }
}());
