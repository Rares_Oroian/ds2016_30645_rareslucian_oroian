(function() {
  angular
    .module('website.api', [])
    .constant('SERVER_DOMAIN', 'http://localhost:8080')
    .constant('LOGIN_ENDPOINT', '/auth/sign/in')
    .constant('LOGOUT_ENDPOINT', '/auth/sign/out')
    .constant('CREATE_BOOK', '/books/')
    .constant('GET_BOOK', '/books/')
    .constant('GET_ALL_BOOKS', '/books/')
    .constant('GET_ALL_USERS', '/users/')
    .constant('GET_BY_TYPE', '/users')
    .constant('SEND_NOTIFICATION', '/notifications/')
    .constant('GET_ALL_BORROWS', '/borrows/')
    .constant('BORROW_BOOK', '/borrows')
    .constant('TOTAL_STOCK', '/books/stock')
    .constant('TOTAL_BORROWED', '/borrows/total')
    .constant('USER_BORROWS', '/borrows/user');
}());
