(function() {
  'use strict';
  angular
  .module('website.api')
  .service('user', userEndpointFunc);

  userEndpointFunc.$inject = [
    '$http',
    '$q',
    'auth',
    'SERVER_DOMAIN',
    'GET_ALL_USERS',
    'GET_BY_TYPE'
  ];

  function userEndpointFunc(
    $http,
    $q,
    auth,
    SERVER_DOMAIN,
    GET_ALL_USERS,
    GET_BY_TYPE
  ) {
    var MANAGER_TYPE_ID = 1;
    var BORROWER_TYPE_ID = 2;
    var exposedAPI = {
      create: createUser,
      get: getUser,
      findAll: findAll,
      findManagers: findAllManagerUsers,
      findBorrowers: findAllBorrowerUsers,
      delete: deleteUser,
      update: updateUser,
      MANAGER_TYPE_ID: MANAGER_TYPE_ID,
      BORROWER_TYPE_ID: BORROWER_TYPE_ID
    };

    function createUser(user) {
      var postObj = {
        username: user.username,
        password: user.password,
        type: user.type
      };

      var url = auth.authoriseOpUrl(SERVER_DOMAIN + GET_ALL_USERS);

      return $http.post(url, postObj);
    }

    function getUser(id) {
      var url = SERVER_DOMAIN + GET_ALL_USERS + id;

      return $http.get(url).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      })
    }

    function findAll(type) {
      var requestUrl = SERVER_DOMAIN;

      if (!type) {
        requestUrl += GET_ALL_USERS;
        requestUrl = auth.authoriseOpUrl(requestUrl);
      } else if (type == MANAGER_TYPE_ID) {
        requestUrl += GET_BY_TYPE + '?type=' + type;
        requestUrl = auth.authoriseOpUrl(requestUrl, true);
      } else if (type == BORROWER_TYPE_ID) {
        requestUrl += GET_BY_TYPE + '?type=' + type;
        requestUrl = auth.authoriseOpUrl(requestUrl, true);
      }

      return $http.get(requestUrl).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    function findAllManagerUsers() {
      return findAll(MANAGER_TYPE_ID);
    }

    function findAllBorrowerUsers() {
      return findAll(BORROWER_TYPE_ID);
    }

    function deleteUser(id) {
      var url = SERVER_DOMAIN + GET_ALL_USERS + id;

      return $http.delete(auth.authoriseOpUrl(url));
    }

    function updateUser(id, userData) {
      var updateObj = {
        id: id,
        username: userData.username,
        password: userData.password,
        type: userData.type,
        created: userData.created
      };

      var url = auth.authoriseOpUrl(SERVER_DOMAIN + GET_ALL_USERS + id);

      return $http.put(url, updateObj);
    }

    return exposedAPI;
  }
}());
