(function() {
  'use strict';

  angular
  .module('website.api')
  .service('notification', notificationEndpointFunc);

  notificationEndpointFunc.$inject = [
    '$http',
    '$q',
    'auth',
    'SERVER_DOMAIN',
    'SEND_NOTIFICATION'
  ];

  function notificationEndpointFunc(
    $http,
    $q,
    auth,
    SERVER_DOMAIN,
    SEND_NOTIFICATION
  ) {
    var exposedAPI = {
      send: sendNotification
    };

    function sendNotification(userId, mail) {
      var sendObj = {
        title: mail.subject,
        description: mail.body
      };

      var url = auth.authoriseOpUrl(SERVER_DOMAIN + SEND_NOTIFICATION + userId);

      return $http.post(url, sendObj);
    }

    return exposedAPI;
  }
}());
