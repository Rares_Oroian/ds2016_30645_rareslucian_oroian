(function() {
  'use strict';
  angular
  .module('website.api')
  .service('borrow', borrowEndpointFunc);

  borrowEndpointFunc.$inject = [
    '$http',
    '$q',
    'auth',
    'SERVER_DOMAIN',
    'GET_ALL_BORROWS',
    'BORROW_BOOK',
    'TOTAL_BORROWED',
    'USER_BORROWS'
  ];

  function borrowEndpointFunc(
    $http,
    $q,
    auth,
    SERVER_DOMAIN,
    GET_ALL_BORROWS,
    BORROW_BOOK,
    TOTAL_BORROWED,
    USER_BORROWS
  ) {
    var exposedAPI = {
      findAll: findAll,
      borrow: borrow,
      findForUser: findForUser,
      returnBorrow: returnBorrow,
      getTotal: getTotalBorrowed
    };

    function findAll() {
      var url = auth.authoriseOpUrl(SERVER_DOMAIN + GET_ALL_BORROWS);

      return $http.get(url).then(function(resp) {
        for (var i = 0; i < resp.data.length; i++) {
          var borrow = resp.data[i];

          borrow.createdAt = new Date(borrow.createdAt);
          borrow.endsAt = new Date(borrow.endsAt);
        }

        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    function borrow(bookId, userId, days, quantity) {
      var url = SERVER_DOMAIN + BORROW_BOOK + '?';
      url += 'bookId=' + bookId + '&userId=' + userId + '&days=' + days;
      url += '&quantity=' + quantity;
      url = auth.authoriseOpUrl(url, true);

      return $http.post(url, {
        bookId: bookId,
        userId: userId
      });
    }

    function returnBorrow(borrowId) {
      var url = SERVER_DOMAIN +  BORROW_BOOK + '?borrowId=' + borrowId;
      url = auth.authoriseOpUrl(url, true);

      return $http.delete(url);
    }

    function getTotalBorrowed() {
      var url = SERVER_DOMAIN + TOTAL_BORROWED;

      return $http.get(auth.authoriseOpUrl(url)).then(function(resp) {
        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    function findForUser(userId) {
      var url = SERVER_DOMAIN + USER_BORROWS + '?userId=' + userId;

      return $http.get(auth.authoriseOpUrl(url, true)).then(function(resp) {
        for (var i = 0; i < resp.data.length; i++) {
          var borrow = resp.data[i];

          borrow.createdAt = new Date(borrow.createdAt);
          borrow.endsAt = new Date(borrow.endsAt);
        }

        return $q.when(resp.data);
      }, function(err) {
        return $q.reject(err);
      });
    }

    return exposedAPI;
  }
}());
