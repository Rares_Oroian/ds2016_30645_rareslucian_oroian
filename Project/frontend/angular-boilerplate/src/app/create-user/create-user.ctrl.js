(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('CreateUserCtrl', createUserCtrlFunc);

  createUserCtrlFunc.$inject = ['$scope', 'user'];

  function createUserCtrlFunc($scope, user) {

    $scope.createUser = function(userData) {
      $scope.successMsg = '';
      $scope.errorMsg = '';

      userData.type = $("input:radio[name ='radios']:checked").val();

      if (!userData.username || !userData.password || !userData.type) {
        return $scope.errorMsg = 'Incomplete form!';
      }

      user.create(userData).then(function(resp) {
        $scope.successMsg = 'User created!';
      }, function(err) {
        $scope.errorMsg = 'Failed to create user!';
      });
    }
  }
}());
