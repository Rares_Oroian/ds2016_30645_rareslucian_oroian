'use strict';

angular.module('test', [
  'ngTouch',
  'restangular',
  'ui.router',
  'ui.bootstrap',
  'chart.js',

  'website.login',
  'website.api',
  'website.common',
  'website.manager-dashboard',
  'website.borrower-dashboard'
]).config(configFunction).run(runFunc);

function configFunction($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
      })
      .state('manager-dashboard', {
        url: '/manager/dashboard',
        templateUrl: 'app/manager-dashboard/manager-dashboard.html',
        controller: 'ManagerDashboardCtrl'
      })
      .state('create-book', {
        url: '/book/create',
        templateUrl: 'app/create-book/create-book.html',
        controller: 'CreateBookCtrl'
      })
      .state('update-book', {
        url: '/books/:bookId/update',
        templateUrl: 'app/update-book/update-book.html',
        controller: 'UpdateBookCtrl',
        resolve: {
          bookIdentifier: ['$stateParams', function(stateParams) {
            return stateParams.bookId;
          }]
        }
      })
      .state('search-books', {
        url: '/books',
        templateUrl: 'app/search-books/search-books.html',
        controller: 'SearchBooksCtrl'
      })
      .state('search-users', {
        url: '/users',
        templateUrl: 'app/search-users/search-users.html',
        controller: 'SearchUsersCtrl'
      })
      .state('update-user', {
        url: '/users/:userId/update',
        templateUrl: 'app/update-user/update-user.html',
        controller: 'UpdateUserCtrl',
        resolve: {
          userId: ['$stateParams', function($stateParams) {
            return $stateParams.userId;
          }]
        }
      })
      .state('send-notification', {
        url: '/notifications/user/:userId',
        controller: 'SendNotificationCtrl',
        templateUrl: 'app/send-notification/send-notification.html',
        resolve: {
          userId: ['$stateParams', function($stateParams) {
            return $stateParams.userId;
          }]
        }
      })
      .state('create-user', {
        url: '/users/create',
        controller: 'CreateUserCtrl',
        templateUrl: 'app/create-user/create-user.html'
      })
      .state('search-borrows', {
        url: '/borrows',
        controller: 'SearchBorrowsCtrl',
        templateUrl: 'app/search-borrows/search-borrows.html'
      })
      .state('lend-book', {
        url: '/borrow',
        controller: 'LendBookCtrl',
        templateUrl: 'app/lend-book/lend-book.html'
      })
      .state('borrower-dashboard', {
        url: '/borrower/dashboard',
        controller: 'BorrowerDashboardCtrl',
        templateUrl: 'app/borrower-dashboard/borrower-dashboard.html'
      });

    $urlRouterProvider.otherwise('/login');
};

runFunc.$inject = ['$rootScope', '$state', 'storage'];

function runFunc($rootScope, $state, storage) {
  $rootScope.$on('$stateChangeStart', function(
    event,
    toState,
    toParams,
    fromState,
    fromParams
  ) {
    var secrets = storage.loadSecrets();

    if (toState.name !== 'borrower-dashboard' &&
        !secrets.isManager &&
        toState.name !== 'login'
      ) {
      event.preventDefault();
      $state.go('login');
    }
  });
}
