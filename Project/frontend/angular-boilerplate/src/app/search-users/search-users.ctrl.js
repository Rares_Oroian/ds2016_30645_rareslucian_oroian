(function() {
  'use strict';
  angular
  .module('website.manager-dashboard')
  .controller('SearchUsersCtrl', searchUsersCtrlFunc);

  searchUsersCtrlFunc.$inject = ['$scope', 'user'];

  function searchUsersCtrlFunc($scope, user) {
    $scope.usersArr = [];
    $scope.visibleUsers = [];
    $scope.BORROWER_TYPE_ID = user.BORROWER_TYPE_ID;
    $scope.MANAGER_TYPE_ID = user.MANAGER_TYPE_ID;

    $scope.find = function(substring) {
      var selectedUsers = [];

      if (!substring) {
        return $scope.visibleUsers = $scope.usersArr;
      }

      for (var i = 0; i < $scope.usersArr.length; i++) {
        var user = $scope.usersArr[i];

        if (user.username.indexOf(substring) >= 0) {
          selectedUsers.push(user);
        }
      }

      $scope.visibleUsers = selectedUsers;
    }

    $scope.findAll = function() {
      user.findAll().then(function(usersArr) {
        usersArr = instantiateDates(usersArr);
        $scope.usersArr = usersArr;
        $scope.visibleUsers = usersArr;
      }, function(err) {
        alert('Failed to fetch all users!');
      });
    }

    $scope.findManagers = function() {
      user.findManagers().then(function(managersArr) {
        managersArr = instantiateDates(managersArr);
        $scope.usersArr = managersArr;
        $scope.visibleUsers = managersArr;
      }, function(err) {
        alert('Failed to fetch all manager users!');
      });
    }

    $scope.findBorrowers = function() {
      user.findBorrowers().then(function(borrowersArr) {
        borrowersArr = instantiateDates(borrowersArr);
        $scope.usersArr = borrowersArr;
        $scope.visibleUsers = borrowersArr;
      }, function(err) {
        alert('Failed to fetch all borrower users!');
      });
    }

    $scope.deleteUser = function(id) {
      user.delete(id).then(function() {
        $scope.findAll();
      }, function(err) {
        alert('User delete failed!');
      });
    }

    function instantiateDates(usersArr) {
      for (var i = 0; i < usersArr.length; i++) {
        usersArr[i].created = new Date(usersArr[i].created);
      }

      return usersArr;
    }

    $scope.findAll();
  }
}());
