package spring.demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.BorrowedBooksDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.dto.UserDTO;
import spring.demo.entities.User;
import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.InvalidSecretException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository usrRepository;
	@Autowired
    private BorrowedBooksService borrowedBooksService;
	@Autowired
    private AuthService authService;

	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findByIduser(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}

		UserDTO dto = new UserDTO.Builder()
						.id(usr.getIduser())
                        .username(usr.getUsername())
                        .password(usr.getPassword())
                        .type(usr.getType())
                        .created(usr.getCreated())
						.create();
		return dto;
	}

	public UserDTO findUserByUsername(String username) {
	    User usr = usrRepository.findByUsername(username);

        if (usr == null) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }

        UserDTO dto = new UserDTO.Builder()
                .id(usr.getIduser())
                .username(usr.getUsername())
                .password(usr.getPassword())
                .type(usr.getType())
                .created(usr.getCreated())
                .create();

        return dto;
    }
	
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();

		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			UserDTO dto = new UserDTO.Builder()
						.id(user.getIduser())
						.username(user.getUsername())
						.password(user.getPassword())
						.type(user.getType())
                        .created(user.getCreated())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public List<UserDTO> findByType(int type) {
	    List<User> users = usrRepository.findByType(type);

	    List<UserDTO> toReturn = new ArrayList<UserDTO>();
	    for (User user : users) {
	        UserDTO dto = new UserDTO.Builder()
                    .id(user.getIduser())
                    .username(user.getUsername())
                    .password(user.getPassword())
					.type(user.getType())
                    .created(user.getCreated())
                    .create();
	        toReturn.add(dto);
        }

        return toReturn;
    }

	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setIduser(userDTO.getId());
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setType(userDTO.getType());
		user.setCreated(new Date());

		User usr = usrRepository.save(user);
		return usr.getIduser();
	}

	public List<BorrowedBooksDTO> listBorrowedBooks(SecretDTO secret, Integer userId) {
        if (authService.isAuthorised(secret)) {
            User user = usrRepository.findByIduser(secret.getUserId());

            if (userId == secret.getUserId() || user.getType() == 1) {
                return borrowedBooksService.findByUserId(userId);
            }
        }
        throw new InvalidSecretException("Invalid secrets.");
	}

    public UserDTO updateUser(Integer userId, UserDTO newUserData, SecretDTO secrets) {
        if (authService.isAuthorised((secrets))) {
            UserDTO user = this.findUserById(userId);

            if (user.getType() == 1) {
                User userObj = new User();

                userObj.setIduser(userId);
                userObj.setUsername(newUserData.getUsername());
                userObj.setPassword(newUserData.getPassword());
                userObj.setType(newUserData.getType());
                userObj.setCreated(new Date());

                userObj = usrRepository.save(userObj);

                return new UserDTO.Builder()
                        .username(userObj.getUsername())
                        .password(userObj.getPassword())
                        .type(userObj.getType())
                        .created(userObj.getCreated())
                        .create();
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }

	public boolean deleteUser(Integer userId, SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            UserDTO user = this.findUserById(secrets.getUserId());

            if (user.getType() == 1) {
                usrRepository.delete(userId);

                return true;
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }


    public List<String> validateUser(UserDTO user) {
	    List<String> errors = new ArrayList<String>();

        if(user.getUsername() == null) {
            errors.add("Username must not be null.");
        }

        if (user.getPassword() == null) {
            errors.add("Password must not be null.");
        }

        if (user.getType() < 1 || user.getType() > 2) {
            errors.add("User type must be one of the following: 1, 2.");
        }

        return errors;
    }
}
