package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.BookDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.dto.UserDTO;
import spring.demo.entities.Book;
import spring.demo.entities.Secret;
import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.InvalidSecretException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.BookRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rares on 12/18/2016.
 */
@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;

    public BookDTO findById(Integer id) {
        Book book = bookRepository.findByIdbook(id);

        if (book == null) {
            throw new ResourceNotFoundException(Book.class.getSimpleName());
        }

        BookDTO dto = new BookDTO.Builder()
                .id(book.getIdbook())
                .bookName(book.getName())
                .bookAuthor(book.getAuthor())
                .availableStock(book.getAvailableStock())
                .totalStock(book.getTotalStock())
                .created(book.getCreated().toString())
                .create();

        return dto;
    }

    public List<BookDTO> findAll() {
        List<Book> books = bookRepository.findAll();
        List<BookDTO> toReturn = new ArrayList<>();

        for (Book book: books) {
            BookDTO dto = new BookDTO.Builder()
                .id(book.getIdbook())
                .bookName(book.getName())
                .bookAuthor(book.getAuthor())
                .availableStock(book.getAvailableStock())
                .totalStock(book.getTotalStock())
                .created(book.getCreated().toString())
                .create();

            toReturn.add(dto);
        }

        return toReturn;
    }

    public Integer create(BookDTO bookDTO) {
        List<String> validationErrors = validateBook(bookDTO);
        if (!validationErrors.isEmpty()) {
            throw new EntityValidationException(Book.class.getSimpleName(),validationErrors);
        }

        Book book = new Book();
        book.setName(bookDTO.getName());
        book.setAuthor(bookDTO.getAuthor());
        book.setAvailableStock(bookDTO.getAvailableStock());
        book.setTotalStock(bookDTO.getTotalStock());
        book.setCreated(new Date());

        Book result = bookRepository.save(book);

        return result.getIdbook();
    }

    public BookDTO update(int id, BookDTO bookDTO, SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            UserDTO user = userService.findUserById(secrets.getUserId());

            if (user.getType() == 1) {
                List<String> validationErrors = validateBook(bookDTO);
                if (!validationErrors.isEmpty()) {
                    throw new EntityValidationException(Book.class.getSimpleName(),validationErrors);
                }

                Book book = new Book();
                book.setIdbook(id);
                book.setName(bookDTO.getName());
                book.setAuthor(bookDTO.getAuthor());
                book.setAvailableStock(bookDTO.getAvailableStock());
                book.setTotalStock(bookDTO.getTotalStock());
                book.setCreated(new Date());

                Book result = bookRepository.save(book);

                BookDTO updatedBook = new BookDTO.Builder()
                        .id(result.getIdbook())
                        .bookName(result.getName())
                        .bookAuthor(result.getAuthor())
                        .availableStock(result.getAvailableStock())
                        .totalStock(result.getTotalStock())
                        .created(result.getCreated().toString())
                        .create();

                return updatedBook;
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }

    public boolean delete(int id, SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            UserDTO user = userService.findUserById(secrets.getUserId());
            if (user.getType() == 1) {
                bookRepository.delete(id);

                return true;
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }

    public Integer getTotalStock(SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            UserDTO user = userService.findUserById(secrets.getUserId());

            if (user.getType() == 1) { // is manager
                return bookRepository.getTotalStock();
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }

    public List<String> validateBook(BookDTO book) {
        List<String> errors = new ArrayList<String>();

        if (book.getName() == null) {
            errors.add("Book name must not be null.");
        }
        if (book.getAuthor() == null) {
            errors.add("Author name must not be null.");
        }
        if (book.getAvailableStock() < 0) {
            errors.add("Available stock must be >= 0.");
        }
        if (book.getTotalStock() < 0) {
            errors.add("Total stock must pe >= 0.");
        }

        return errors;
    }
}
