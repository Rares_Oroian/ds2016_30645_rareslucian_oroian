package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.BookDTO;
import spring.demo.dto.BorrowedBooksDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.dto.UserDTO;
import spring.demo.entities.Book;
import spring.demo.entities.BorrowedBooks;
import spring.demo.entities.User;
import spring.demo.errorhandler.InvalidSecretException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.misc.Mail;
import spring.demo.repositories.BookRepository;
import spring.demo.repositories.BorrowedBooksRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Rares on 12/22/2016.
 */

@Service
public class BorrowedBooksService {
    @Autowired
    private BorrowedBooksRepository borrowedBooksRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthService authService;
    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;
    @Autowired
    private NotificationsService notificationsService;

    public BorrowedBooksDTO findById(Integer id) {
        BorrowedBooks borrowedBooks = borrowedBooksRepository.findById(id);

         if (borrowedBooks == null) {
             throw new ResourceNotFoundException(BorrowedBooks.class.getSimpleName());
         }

         BorrowedBooksDTO toReturn = new BorrowedBooksDTO.Builder()
            .id(borrowedBooks.getId())
            .user(borrowedBooks.getIdUser())
            .idBook(borrowedBooks.getIdBook())
            .createdAt(borrowedBooks.getCreatedAt())
            .endsAt(borrowedBooks.getEndsAt())
            .quantity(borrowedBooks.getQuantity())
            .create();

         return toReturn;
    }

    public List<BorrowedBooksDTO> findAll() {
        List<BorrowedBooks> borrows = borrowedBooksRepository.findAll();
        List<BorrowedBooksDTO> toReturn = new ArrayList<>();

        for (BorrowedBooks b : borrows) {
            UserDTO user = userService.findUserById(b.getIdUser());
            BookDTO book = bookService.findById(b.getIdBook());

            BorrowedBooksDTO dto = new BorrowedBooksDTO.Builder()
                    .id(b.getId())
                    .user(b.getIdUser())
                    .idBook(b.getIdBook())
                    .createdAt(b.getCreatedAt())
                    .endsAt(b.getEndsAt())
                    .quantity(b.getQuantity())
                    .username(user.getUsername())
                    .bookName(book.getName())
                    .availableStock(book.getAvailableStock())
                    .create();

            toReturn.add(dto);
        }

        return toReturn;
    }

    public List<BorrowedBooksDTO> findByUserId(Integer userId) {
        List<BorrowedBooks> borrows = borrowedBooksRepository.findByIdUser(userId);
        List<BorrowedBooksDTO> toReturn = new ArrayList<>();

        for (BorrowedBooks b : borrows) {
            BookDTO book = bookService.findById(b.getIdBook());

            BorrowedBooksDTO dto = new BorrowedBooksDTO.Builder()
                    .id(b.getId())
                    .user(b.getIdUser())
                    .idBook(b.getIdBook())
                    .bookName(book.getName())
                    .createdAt(b.getCreatedAt())
                    .endsAt(b.getEndsAt())
                    .quantity(b.getQuantity())
                    .create();

            toReturn.add(dto);
        }

        return toReturn;
    }

    public List<BorrowedBooksDTO> findByBookId(Integer bookId) {
        List<BorrowedBooks> borrows = borrowedBooksRepository.findByIdBook(bookId);
        List<BorrowedBooksDTO> toReturn = new ArrayList<>();

        for (BorrowedBooks b : borrows) {
            BorrowedBooksDTO dto = new BorrowedBooksDTO.Builder()
                    .id(b.getId())
                    .user(b.getIdUser())
                    .idBook(b.getIdBook())
                    .createdAt(b.getCreatedAt())
                    .endsAt(b.getEndsAt())
                    .quantity(b.getQuantity())
                    .create();

            toReturn.add(dto);
        }

        return toReturn;
    }

    public List<BorrowedBooks> findBooksForFirstNotification() {
        Calendar rightNow = Calendar.getInstance();
        rightNow.add(Calendar.DATE, -3);

        List<BorrowedBooks> needFistNotification = borrowedBooksRepository.findByExpireTimeAndNotificationsCnt(
                rightNow.getTime(),
                0
        );

        return needFistNotification;
    }

    public List<BorrowedBooks> findBooksForSecondNotification() {
        Calendar rightNow = Calendar.getInstance();
        rightNow.add(Calendar.DATE, -1);

        List<BorrowedBooks> needSecondNotification = borrowedBooksRepository.findByExpireTimeAndNotificationsCnt(
                rightNow.getTime(),
                1
        );

        return needSecondNotification;
    }

    public void incrementNotificationsCnt(BorrowedBooks b) {
        b.setSystemNotificationsCnt(b.getSystemNotificationsCnt() + 1);
        borrowedBooksRepository.save(b);
    }

    public boolean borrow(int bookId, int userId, int days, int quantity, SecretDTO secrets) throws ParseException {
        if (authService.isAuthorised(secrets)) {
            Book book = bookRepository.findByIdbook(bookId);

            if (book.getAvailableStock() >= quantity) {
                book.setAvailableStock(book.getAvailableStock() - quantity);
                bookRepository.save(book);
            } else {
                throw new InvalidSecretException("Unauthorised");
            }

            BorrowedBooks borrow = new BorrowedBooks();
            borrow.setIdBook(bookId);
            borrow.setIdUser(userId);
            borrow.setSystemNotificationsCnt(0);
            borrow.setQuantity(quantity);

            Calendar rightNow = Calendar.getInstance();
            borrow.setCreatedAt(rightNow.getTime());
            rightNow.add(Calendar.DATE, days);
            borrow.setEndsAt(rightNow.getTime());

            borrowedBooksRepository.save(borrow);

            BookDTO bookDTO = bookService.findById(bookId);
            String mailSubject = "[Notification] You just borrowed a book";
            String mailDescription = "You borrowed (" + bookDTO.getName() + ") for a period of " + days + " days.\n" +
                    "The book needs to be returned on " + rightNow.getTime().toString() + ".\n\n" +
                    "Have a nice read time!";

            notificationsService.sendMailToUser(userId, new Mail(mailSubject, mailDescription), secrets);

            return true;
        }

        throw new InvalidSecretException("Unauthorised");
    }

    public void returnBook(int idBorrow, SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            BorrowedBooks borrow = borrowedBooksRepository.findById(idBorrow);
            Book book = bookRepository.findByIdbook(borrow.getIdBook());

            book.setAvailableStock(book.getAvailableStock() + borrow.getQuantity());
            bookRepository.save(book);

            borrowedBooksRepository.delete(idBorrow);

            return;
        }

        throw new InvalidSecretException("Unauthorised");
    }

    public Integer countTotalBorrowed(SecretDTO secrets) {
        if (authService.isAuthorised(secrets)) {
            UserDTO user = userService.findUserById(secrets.getUserId());

            if (user.getType() == 1) { // is manager
                return borrowedBooksRepository.calculateTotalBorrowed();
            }
        }

        throw new InvalidSecretException("Unauthorised");
    }
}
