package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.AuthDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.entities.Secret;
import spring.demo.entities.User;
import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.InvalidSecretException;
import spring.demo.repositories.SecretRepository;
import spring.demo.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rares on 12/28/2016.
 */
@Service
public class AuthService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SecretRepository secretRepository;

    public AuthDTO signIn(AuthDTO credentials) {
        User user = userRepository.findByUsername(credentials.getUsername());

        if (user == null) {
            throw new InvalidSecretException("Unauthorised.");
        } else if (!user.getPassword().equals(credentials.getPassword())) {
            throw new InvalidSecretException("Unauthorised.");
        } else {
            Secret secret = secretRepository.findByIdUser(user.getIduser());

            if (secret == null) {
                secret = new Secret(user.getIduser());
                secret = secretRepository.save(secret);
            }

            SecretDTO dto = new SecretDTO.Builder()
                    .idSecret(secret.getSecret())
                    .idUser(secret.getIdUser())
                    .create();

            return new AuthDTO(
                    user.getUsername(),
                    null,
                    "Operation successful!",
                    dto,
                    user.getType() == 1
            );
        }
    }

    public boolean signOut(SecretDTO dto) {
        List<String> errors = validateSecretDTO(dto);

        if (errors.size() > 0) {
            throw new EntityValidationException("Signout refused", errors);
        }

        if (isAuthorised(dto)) {
            secretRepository.delete(dto.getSecret());
            return true;
        } else {
            throw new InvalidSecretException("Invalid secrets.");
        }
    }

    public boolean isAuthorised(SecretDTO dto) {
        Secret secret = secretRepository.findBySecret(dto.getSecret());

        if (secret == null) {
            return false;
        }

        return (int)secret.getIdUser() == dto.getUserId();
    }

    public List<String> validateSecretDTO(SecretDTO dto) {
        List<String> errors = new ArrayList<>();

        if (dto == null) {
            errors.add("User secret is required.");
        } else {
            if (dto.getSecret() <= 0) {
                errors.add("Invalid secret.");
            }
            if (dto.getUserId() <= 0) {
                errors.add("Invalid user id.");
            }
        }

        return errors;
    }
}
