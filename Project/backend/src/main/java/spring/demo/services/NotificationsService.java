package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.BookDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.dto.UserDTO;
import spring.demo.entities.BorrowedBooks;
import spring.demo.entities.Secret;
import spring.demo.entities.User;
import spring.demo.errorhandler.InvalidSecretException;
import spring.demo.misc.Mail;
import spring.demo.misc.MailService;

import javax.persistence.EntityNotFoundException;

/**
 * Created by Rares on 1/4/2017.
 */
@Service
public class NotificationsService {
    @Autowired
    AuthService authService;
    @Autowired
    UserService userService;
    @Autowired
    BookService bookService;
    @Autowired
    BorrowedBooksService borrowedBooksService;

    MailService mailService = new MailService("rares94test@gmail.com","rares94test1234");

    public boolean sendMailToUser(int targetUserId, Mail mail, SecretDTO secret) {
        if (authService.isAuthorised(secret)) {
            UserDTO user = userService.findUserById(secret.getUserId());

            if (user.getType() == 2) {
                throw new InvalidSecretException("Not permitted.");
            }

            UserDTO targetUser = userService.findUserById(targetUserId);
            if (targetUser == null) {
                throw new EntityNotFoundException("Could not find account with that id.");
            }

            mailService.sendMail(targetUser.getUsername(), mail.getTitle(), mail.getDescription());

            return true;
        } else {
            throw new InvalidSecretException("Invalid credentials.");
        }
    }

    public boolean systemSendMailToUser(BorrowedBooks borrow, Mail mail) {
        int idUser = borrow.getIdUser();
        UserDTO targetUser = userService.findUserById(idUser);
        int idBook = borrow.getIdBook();
        BookDTO targetBook = bookService.findById(idBook);

        String description = mail.getDescription();
        description += "\nYou have " + borrow.getQuantity() + " piece(s) of " + targetBook.getName() + " of " + targetBook.getAuthor();
        description += "\n\nYour favourite library app!";
        mail.setDescription(description);
        mailService.sendMail(targetUser.getUsername(), mail.getTitle(), mail.getDescription());

        return true;
    }
}
