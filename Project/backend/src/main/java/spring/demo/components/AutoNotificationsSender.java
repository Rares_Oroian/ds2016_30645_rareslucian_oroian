package spring.demo.components;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spring.demo.controller.BorrowedBooksController;
import spring.demo.entities.BorrowedBooks;
import spring.demo.misc.Mail;
import spring.demo.misc.MailService;
import spring.demo.services.BorrowedBooksService;
import spring.demo.services.NotificationsService;

import java.util.Date;
import java.util.List;

/**
 * Created by Rares on 1/4/2017.
 */
@EnableScheduling
@Component
public class AutoNotificationsSender {
    @Autowired
    BorrowedBooksService borrowedBooksService;
    @Autowired
    NotificationsService notificationsService;

    @Scheduled(fixedRate = 60 * 1000) // every minute
    public void keepAlive() {
        Date now = new Date();
        System.out.println(now.toString() + ": Running automatic notifications task");

        sendNotificationsWhereNeeded();

        now = new Date();
        System.out.println(now.toString() + ": Finished");
    }

    public void sendNotificationsWhereNeeded() {
        List<BorrowedBooks> needFirst = borrowedBooksService.findBooksForFirstNotification();
        System.out.println("Found borrows needing first notification: " + needFirst.size());
        sendFirstNotifications(needFirst);

        List<BorrowedBooks> needSecond = borrowedBooksService.findBooksForSecondNotification();
        System.out.println("Found borrows needing second notification: " + needSecond.size());
        sendSecondNotification(needSecond);
    }

    public void sendFirstNotifications(List<BorrowedBooks> borrows) {
        String mailTitle = "Your borrowed book term is approaching";

        for (BorrowedBooks b : borrows) {
            String mailDescription = "You must return you book in 3 days.";

            notificationsService.systemSendMailToUser(b, new Mail(mailTitle, mailDescription));
            borrowedBooksService.incrementNotificationsCnt(b);
        }
    }

    public void sendSecondNotification(List<BorrowedBooks> borrows) {
        String mailTitle = "Your borrowed book term is approaching, 1 day left!";

        for (BorrowedBooks b : borrows) {
            String mailDescription = "You must return you book in 1 day.";

            notificationsService.systemSendMailToUser(b, new Mail(mailTitle, mailDescription));
            borrowedBooksService.incrementNotificationsCnt(b);
        }
    }
}
