package spring.demo.web.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.EnumSet;

public class SpringWebAppInitializer implements WebApplicationInitializer {

	private static final String CONFIG_LOCATION = "spring.demo.config";
	private static final String MAPPING_URL = "/*";

	public void onStartup(ServletContext servletContext) throws ServletException {
		WebApplicationContext context = getContext();
		servletContext.addListener(new ContextLoaderListener(context));
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(
				context));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping(MAPPING_URL);

		FilterRegistration.Dynamic filterRegistration = servletContext.addFilter("Basic CORS Filter", new Filter() {
			@Override
			public void init(FilterConfig filterConfig) throws ServletException {

			}

			@Override
			public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
				HttpServletRequest request = (HttpServletRequest) req;
				HttpServletResponse response = (HttpServletResponse) res;

				response.addHeader("Access-Control-Allow-Origin", "*");
				if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
					// CORS "pre-flight" request
					response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
					response.addHeader("Access-Control-Allow-Headers", "Content-Type");
					response.addHeader("Access-Control-Max-Age", "1");
				}

				chain.doFilter(req, res);
			}

			@Override
			public void destroy() {

			}
		});
		filterRegistration.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		System.out.println(filterRegistration);
	}

	private AnnotationConfigWebApplicationContext getContext() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setConfigLocation(CONFIG_LOCATION);
		return context;
	}

}
//
//public class SpringWebAppInitializer implements WebApplicationInitializer {
//
//	private static final String CONFIG_LOCATION = "spring.demo.config";
//	private static final String MAPPING_URL = "/*";
//
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		WebApplicationContext context = getContext();
//		servletContext.addListener(new ContextLoaderListener(context));
//		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(
//				context));
//		dispatcher.setLoadOnStartup(1);
//		dispatcher.addMapping(MAPPING_URL);
//	}
//
//	private AnnotationConfigWebApplicationContext getContext() {
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//		context.setConfigLocation(CONFIG_LOCATION);
//		return context;
//	}
//
//}
