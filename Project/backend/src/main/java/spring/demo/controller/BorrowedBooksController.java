package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.BorrowedBooksDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.services.BorrowedBooksService;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Rares on 12/22/2016.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/borrows")
public class BorrowedBooksController {
    @Autowired
    BorrowedBooksService borrowedBooksService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<BorrowedBooksDTO> getAllBorrows() {
        return borrowedBooksService.findAll();
    }

    @RequestMapping(value = "/user", params = {"userId"}, method = RequestMethod.GET)
    public List<BorrowedBooksDTO> getAllBorrowsOfUser(@RequestParam(value = "userId") Integer userId) {
        return borrowedBooksService.findByUserId(userId);
    }

    @RequestMapping(value = "", params = {"bookId"}, method = RequestMethod.GET)
    public List<BorrowedBooksDTO> getAllBorrowsOfBook(@RequestParam(value = "bookId") Integer bookId) {
        return borrowedBooksService.findByBookId(bookId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BorrowedBooksDTO getBorrowedBookById(@PathVariable("id") int id) { return borrowedBooksService.findById(id); }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Boolean borrowBook(
            @RequestParam(value = "userId") int userId,
            @RequestParam(value = "bookId") int bookId,
            @RequestParam(value = "days") int days,
            @RequestParam(value = "quantity") int quantity,
            @RequestParam(value = "key") int key,
            @RequestParam(value = "value") int value
    ) {
        try {
            return borrowedBooksService.borrow(bookId, userId, days, quantity, new SecretDTO(key, value));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public void returnBook(
        @RequestParam(value = "borrowId") int borrowId,
        @RequestParam(value = "key") int key,
        @RequestParam(value = "value") int value
    ) {
        borrowedBooksService.returnBook(borrowId, new SecretDTO(key, value));
    }

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public Integer countTotalBorrowed(
            @RequestParam(value = "key") int key,
            @RequestParam(value = "value") int value
    ) {
        return borrowedBooksService.countTotalBorrowed(new SecretDTO(key, value));
    }
}
