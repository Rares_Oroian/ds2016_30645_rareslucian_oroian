package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.AuthDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.services.AuthService;

/**
 * Created by Rares on 12/28/2016.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/sign/in", method = RequestMethod.POST)
    public AuthDTO signInUser(@RequestBody AuthDTO authDTO) {
        return authService.signIn(authDTO);
    }

    @RequestMapping(value = "/sign/out", method = RequestMethod.POST)
    public boolean signOutUser(@RequestBody SecretDTO secretDTO) {
        return authService.signOut(secretDTO);
    }
}
