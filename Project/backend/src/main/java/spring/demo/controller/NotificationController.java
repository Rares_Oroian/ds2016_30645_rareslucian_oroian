package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.SecretDTO;
import spring.demo.entities.Secret;
import spring.demo.misc.Mail;
import spring.demo.misc.MailService;
import spring.demo.services.NotificationsService;

/**
 * Created by Rares on 1/4/2017.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/notifications")
public class NotificationController {
    @Autowired
    NotificationsService notificationsService;

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public boolean sendMailNotification(
            @RequestBody Mail mail,
            @PathVariable("userId") int targetUserId,
            @RequestParam(value = "key") int secretId,
            @RequestParam(value = "value") int userId
    ) {
       return notificationsService.sendMailToUser(targetUserId, mail, new SecretDTO(secretId, userId));
    }
}