package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.BorrowedBooksDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("id") int id) {
		return userService.findUserById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers() {
		return userService.findAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public int insertUser(@RequestBody UserDTO userDTO) {
		return userService.create(userDTO);
	}

	@RequestMapping(value = "", params = {"username"}, method = RequestMethod.GET)
	public UserDTO getByUsername(@RequestParam(value = "username") String username) {
		return userService.findUserByUsername(username);
	}

	@RequestMapping(value = "", params = {"type"}, method = RequestMethod.GET)
	public List<UserDTO> getByType(@RequestParam(value = "type") String type) {
		return userService.findByType(Integer.parseInt(type));
	}

	@RequestMapping(value = "/{id}/books", method = RequestMethod.GET)
	public List<BorrowedBooksDTO> listBorrowedBooks(
	        @PathVariable("id") int targetUserId,
            @RequestParam(value = "key") int secretKey,
            @RequestParam(value = "value") int userId
    ) {
		return userService.listBorrowedBooks(new SecretDTO(secretKey, userId), targetUserId);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public UserDTO updateUser(
            @PathVariable("id") int targetUserId,
            @RequestParam(value = "key") int secretKey,
            @RequestParam(value = "value") int userId,
            @RequestBody UserDTO newUserData
    ) {
        return userService.updateUser(targetUserId, newUserData, new SecretDTO(secretKey, userId));
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean deleteUser(
			@PathVariable("id") int targetUserId,
			@RequestParam(value = "key") int secretKey,
			@RequestParam(value = "value") int userId
	) {
		return userService.deleteUser(targetUserId, new SecretDTO(secretKey, userId));
	}
}

