package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.BookDTO;
import spring.demo.dto.SecretDTO;
import spring.demo.services.BookService;

import java.util.List;

/**
 * Created by Rares on 12/18/2016.
 */
@CrossOrigin
@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BookDTO getBookById(@PathVariable("id") int id) { return bookService.findById(id); }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<BookDTO> getAllBooks() { return bookService.findAll(); }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Integer createBook(@RequestBody BookDTO bookDTO) { return bookService.create(bookDTO); }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public BookDTO updateBook(
            @PathVariable int id,
            @RequestBody BookDTO updatedBookDTO,
            @RequestParam(value = "key") int secretId,
            @RequestParam(value = "value") int userId
    ) {
        return bookService.update(id, updatedBookDTO, new SecretDTO(secretId, userId));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Boolean deleteBook(
            @PathVariable int id,
            @RequestParam(value = "key") int secretId,
            @RequestParam(value = "value") int userId
    ) {
        return bookService.delete(id, new SecretDTO(secretId, userId));
    }

    @RequestMapping(value = "/stock", method = RequestMethod.GET)
    public Integer getTotalStock(
            @RequestParam(value = "key") int secretId,
            @RequestParam(value = "value") int userId
    ) {
        return bookService.getTotalStock(new SecretDTO(secretId, userId));
    }
}
