package spring.demo.dto;

import java.util.Date;

public class UserDTO {

	private Integer id;
	private String username;
	private String password;
	private Integer type;
	private String created;

	public UserDTO() {
	}

	public UserDTO(Integer id, String username, String password, Integer type, String created) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.type = type;
		this.created = created;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {this.username = username;}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) { this.type = type; }

	public String getCreated() { return created; }

	public void setCreated(String created) { this.created = created; }

	public static class Builder {
		private Integer nestedid;
		private String nestedusername;
		private String nestedpassword;
		private Integer nestedtype;
		private String nestedcreated;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder username(String username) {
			this.nestedusername = username;
			return this;
		}
		
		public Builder password(String password) {
			this.nestedpassword = password;
			return this;
		}

		public Builder type(Integer type) {
			this.nestedtype = type;
			return this;
		}

		public Builder created(Date date) {
		    this.nestedcreated = date.toString();
		    return this;
        }

		public UserDTO create() {
			return new UserDTO(nestedid, nestedusername, nestedpassword, nestedtype, nestedcreated);
		}
	}

}
