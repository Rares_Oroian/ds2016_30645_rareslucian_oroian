package spring.demo.dto;

import java.util.Date;

/**
 * Created by Rares on 12/22/2016.
 */
public class BorrowedBooksDTO {
    private Integer id;
    private Integer user;
    private Integer idBook;
    private String createdAt;
    private String endsAt;
    private Integer quantity;
    private String username;
    private String bookName;
    private Integer availableStock;

    public BorrowedBooksDTO() {}
    public BorrowedBooksDTO(
            Integer id,
            Integer user,
            Integer idBook,
            String createdAt,
            String endsAt,
            Integer quantity,
            String username,
            String bookName,
            Integer stock
        ) {
        this.id = id;
        this.user = user;
        this.idBook = idBook;
        this.createdAt = createdAt;
        this.endsAt = endsAt;
        this.quantity = quantity;
        this.username = username;
        this.bookName = bookName;
        this.availableStock = stock;
    }

    public void setId(Integer id) {this.id = id;}
    public Integer getId() {return id;}

    public void setUser(Integer user) {this.user = user;}
    public Integer getUser() {return user;}

    public void setIdBook(Integer idBook) {this.idBook = idBook;}
    public Integer getIdBook() {return idBook;}

    public void setCreatedAt(String createdAt) {this.createdAt = createdAt;}
    public String getCreatedAt() {return createdAt;}

    public void setEndsAt(String endsAt) {this.endsAt = endsAt;}
    public String getEndsAt() {return endsAt;}

    public void setQuantity(Integer quantity) {this.quantity = quantity;}
    public Integer getQuantity() {return quantity;}

    public void setUsername(String username) {this.username = username;}
    public String getUsername() {return username;}

    public void setBookName(String bookName) {this.bookName = bookName;}
    public String getBookName() {return bookName;}

    public void setAvailableStock(Integer stock) {this.availableStock = stock;}
    public Integer getAvailableStock() {return this.availableStock;}

    public static class Builder {
        private Integer nestedId;
        private Integer nestedUser;
        private Integer nestedIdBook;
        private String nestedCreatedAt;
        private String nestedEndsAt;
        private Integer nestedQuantity;
        private String nestedUsername;
        private String nestedBookName;
        private Integer nestedAvailableStock;

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }
        public Builder user(Integer user) {
            this.nestedUser = user;
            return this;
        }
        public Builder idBook(Integer idBook) {
            this.nestedIdBook = idBook;
            return this;
        }
        public Builder createdAt(Date date) {
            this.nestedCreatedAt = date.toString();
            return this;
        }
        public Builder endsAt(Date date) {
            this.nestedEndsAt = date.toString();
            return this;
        }
        public Builder quantity(Integer q) {
            this.nestedQuantity = q;
            return this;
        }

        public Builder username(String username) {
            this.nestedUsername = username;
            return this;
        }

        public Builder bookName(String bookName) {
            this.nestedBookName = bookName;
            return this;
        }

        public Builder availableStock(Integer stock) {
            this.nestedAvailableStock = stock;
            return this;
        }

        public BorrowedBooksDTO create() {
            return new BorrowedBooksDTO(nestedId, nestedUser, nestedIdBook, nestedCreatedAt, nestedEndsAt, nestedQuantity, nestedUsername, nestedBookName, nestedAvailableStock);
        }
    }


}
