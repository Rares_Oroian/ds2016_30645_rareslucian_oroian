package spring.demo.dto;

/**
 * Created by Rares on 12/28/2016.
 */
public class AuthDTO {
    private String username;
    private String password;
    private String message;
    private SecretDTO secret;
    private Boolean isManager;

    public AuthDTO() {}
    public AuthDTO(String username, String password, String message, SecretDTO secret) {
        this.username = username;
        this.password = password;
        this.message = message;
        this.secret = secret;
    }

    public AuthDTO(String username, String password, String message, SecretDTO secret, Boolean isManager) {
        this.username = username;
        this.password = password;
        this.message = message;
        this.secret = secret;
        this.isManager = isManager;
    }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public String getMessage() { return message; }
    public void setMessage(String message) { this.message = message; }

    public SecretDTO getSecret() { return secret; }
    public void setSecret(SecretDTO secret) { this.secret = secret; };

    public Boolean getIsManager() {return isManager;}
    public void setIsManager(boolean isManager) {this.isManager = isManager;}

    public static class Builder {
        private String nestedUsername;
        private String nestedPassword;
        private String nestedMessage;
        private SecretDTO nestedSecret;

        public Builder username(String username) {
            this.nestedUsername = username;
            return this;
        }
        public Builder password(String password) {
            this.nestedPassword = password;
            return this;
        }
        public Builder message(String message) {
            this.nestedMessage = message;
            return this;
        }

        public Builder secret(SecretDTO secret) {
            this.nestedSecret = secret;
            return this;
        }

        public AuthDTO create() {
            return new AuthDTO(nestedUsername, nestedPassword, nestedMessage, nestedSecret);
        }
    }
}
