package spring.demo.dto;

/**
 * Created by Rares on 1/3/2017.
 */
public class SecretDTO {
    private int idSecret;
    private int userId;

    public SecretDTO() {}
    public SecretDTO(Integer idSecret, Integer idUser) {
        super();
        this.idSecret = idSecret;
        this.userId = idUser;
    }

    public Integer getSecret() { return idSecret; }
    public void setSecret(Integer secret) { this.idSecret = secret; }

    public Integer getUserId() { return userId; }
    public void setUserId(Integer userId) { this.userId = userId; }

    public static class Builder {
        private Integer nestedIdSecret;
        private Integer nestedIdUser;

        public Builder idSecret(Integer idSecret) {
            this.nestedIdSecret = idSecret;
            return this;
        }
        public Builder idUser(Integer idUser) {
            this.nestedIdUser = idUser;
            return this;
        }

        public SecretDTO create() { return new SecretDTO(nestedIdSecret, nestedIdUser); }
    }
}
