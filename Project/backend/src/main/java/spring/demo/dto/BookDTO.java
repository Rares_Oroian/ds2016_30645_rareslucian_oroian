package spring.demo.dto;

/**
 * Created by Rares on 12/18/2016.
 */
public class BookDTO {
    private Integer id;
    private String bookName;
    private String bookAuthor;
    private Integer availableStock;
    private Integer totalStock;
    private String created;

    public BookDTO() {}
    public BookDTO(Integer id, String bookName, String bookAuthor, Integer availableStock, Integer totalStock, String created) {
        super();
        this.id = id;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.availableStock = availableStock;
        this.totalStock = totalStock;
        this.created = created;
    }

    public void setId(Integer id) {this.id = id; }
    public Integer getId() {return id;}

    public void setName(String name) { this.bookName = name; }
    public String getName() { return bookName; }

    public void setAuthor(String author) { this.bookAuthor = author; }
    public String getAuthor() { return bookAuthor; }

    public void setAvailableStock(Integer stock) { this.availableStock = stock; }
    public Integer getAvailableStock() { return availableStock; }

    public void setTotalStock(Integer stock) { this.totalStock = stock; }
    public Integer getTotalStock() { return totalStock; }

    public void setCreated(String created) { this.created = created; }
    public String getCreated() { return created; }


    public static class Builder {
        private Integer nestedId;
        private String nestedName;
        private String nestedAuthor;
        private Integer nestedAvailableStock;
        private Integer nestedTotalStock;
        private String nestedCreated;

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }
        public Builder bookName(String name) {
            this.nestedName = name;
            return this;
        }
        public Builder bookAuthor(String author) {
            this.nestedAuthor = author;
            return this;
        }
        public Builder availableStock(Integer stock) {
            this.nestedAvailableStock = stock;
            return this;
        }
        public Builder totalStock(Integer stock) {
            this.nestedTotalStock = stock;
            return this;
        }
        public Builder created(String created) {
            this.nestedCreated = created;
            return this;
        }

        public BookDTO create() {
            return new BookDTO(nestedId, nestedName, nestedAuthor, nestedAvailableStock, nestedTotalStock, nestedCreated);
        }
    }
}
