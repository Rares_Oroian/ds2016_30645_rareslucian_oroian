package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import spring.demo.entities.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Book findByIdbook(Integer id);

    Book findByName(String bookName);

    Book findByAuthor(String authorName);

    @Query("select sum(b.totalStock) from Book b")
    Integer getTotalStock();
}
