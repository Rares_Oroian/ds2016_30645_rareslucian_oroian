package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Secret;

/**
 * Created by Rares on 1/3/2017.
 */
public interface SecretRepository extends JpaRepository<Secret, Integer> {
    Secret findBySecret(Integer idSecret);

    Secret findByIdUser(Integer idUser);
}
