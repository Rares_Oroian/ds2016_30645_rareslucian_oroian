package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByIduser(int iduser);

    User findByUsername(String username);

    List<User> findByType(int type);
}
