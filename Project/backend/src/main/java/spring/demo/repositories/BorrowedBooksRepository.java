package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import spring.demo.entities.BorrowedBooks;

import java.util.Date;
import java.util.List;

/**
 * Created by Rares on 12/22/2016.
 */
public interface BorrowedBooksRepository extends JpaRepository<BorrowedBooks, Integer> {
    BorrowedBooks findById(Integer id);

    List<BorrowedBooks> findByIdUser(Integer idUser);

    List<BorrowedBooks> findByIdBook(Integer idBook);

    @Query("select b from BorrowedBooks b where b.endsAt > ?1 and b.systemNotificationsCnt = ?2")
    List<BorrowedBooks> findByExpireTimeAndNotificationsCnt(Date timeRef, Integer notificationsCntRef);

    @Query("select sum(b.quantity) from BorrowedBooks b")
    Integer calculateTotalBorrowed();
}
