package spring.demo.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Rares on 1/3/2017.
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Unauthorised")
public class InvalidSecretException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidSecretException(String msg) { super(msg); }
}
