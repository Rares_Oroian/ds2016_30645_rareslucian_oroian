package spring.demo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Rares on 1/3/2017.
 */
@Entity
@Table(name = "secret")
public class Secret implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private int idSecret;
    private int idUser;

    public Secret() {}
    public Secret(Integer idUser) {
        super();
        this.idUser = idUser;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idSecret", unique = true, nullable = false)
    public Integer getSecret() { return idSecret; }
    public void setSecret(Integer secret) { this.idSecret = secret; }

    @Column(name = "idUser", unique = true, nullable = false)
    public Integer getIdUser() { return idUser; }
    public void setIdUser(Integer idUser) { this.idUser = idUser; }
}
