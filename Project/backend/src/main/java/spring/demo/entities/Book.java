package spring.demo.entities;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Rares on 12/18/2016.
 */
@Entity
@Table(name = "book")
public class Book implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private Integer idbook;
    private String name;
    private String author;
    private Integer availableStock;
    private Integer totalStock;
    private Date created;

    public Book() {}
    public Book(String name, String author, Integer availableStock, Integer totalStock, Date created) {
        super();
        this.name = name;
        this.author = author;
        this.availableStock = availableStock;
        this.totalStock = totalStock;
        this.created = created;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idbook", unique = true, nullable = false)
    public Integer getIdbook() { return idbook; }
    public void setIdbook(Integer id) { this.idbook = id; }

    @Column(name = "name", nullable = false, length = 150)
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    @Column(name = "author", nullable = false, length = 100)
    public String getAuthor() { return author; }
    public void setAuthor(String author) { this.author = author; }

    @Column(name = "availableStock")
    public Integer getAvailableStock() { return availableStock; }
    public void setAvailableStock(Integer availableStock) { this.availableStock = availableStock; }

    @Column(name = "totalStock")
    public Integer getTotalStock() { return totalStock; }
    public void setTotalStock(Integer totalStock) { this.totalStock = totalStock; }

    @Column(name = "created")
    public Date getCreated() { return created; }
    public void setCreated(Date created) { this.created = created; }
}
