package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer iduser;
	private String username;
	private String password;
	private int type;
    private Date created;

	public User() {
	}

	public User(Integer iduser, String username, String password, int type, Date created) {
		super();
		this.iduser = iduser;
		this.username = username;
		this.password = password;
		this.type = type;
		this.created = created;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "iduser", unique = true, nullable = false)
	public Integer getIduser() { return this.iduser; }
	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	@Column(name = "username", unique = true, nullable = false, length = 45)
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = true, length = 45)
	public String getPassword() {return this.password;}
	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "type", nullable = true)
	public Integer getType() {
		return this.type;
	}
	public void setType(Integer type) {
		this.type = type;
	}

    @Column(name = "created", nullable = false, length = 100)
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
}
