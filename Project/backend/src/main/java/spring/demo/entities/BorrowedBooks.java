package spring.demo.entities;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Rares on 12/22/2016.
 */
@Entity
@Table(name = "borrowedbooks")
public class BorrowedBooks implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

//    @ManyToOne(targetEntity = User.class)
//    @JoinColumn(name="iduser")
//    private User user = new User();
    private Integer idUser;

//    @ManyToOne(targetEntity = Book.class)
//    @JoinColumn(name = "idbook")
//    private Book book;
    private Integer idBook;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "endsAt")
    private Date endsAt;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "systemNotificationCnt")
    private Integer systemNotificationsCnt;

    public BorrowedBooks() {}
    public BorrowedBooks(Integer idUser, Integer idBook, Date createdAt, Date endsAt,Integer quantity, Integer notificationsCnt) {
        this.idUser = idUser;
        this.idBook = idBook;
        this.createdAt = createdAt;
        this.endsAt = endsAt;
        this.quantity = quantity;
        this.systemNotificationsCnt = notificationsCnt;
    }

    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}

    public Integer getIdUser() {return idUser;}
    public void setIdUser(Integer idUser) {this.idUser = idUser;}

    public Integer getIdBook() {return idBook;}
    public void setIdBook(Integer idBook) {this.idBook = idBook;}

    public Date getCreatedAt() {return createdAt;}
    public void setCreatedAt(Date createdAt) {this.createdAt = createdAt;}

    public Date getEndsAt() {return endsAt;}
    public void setEndsAt(Date endsAt) {this.endsAt = endsAt;}

    public Integer getQuantity() {return quantity;}
    public void setQuantity(Integer quantity) {this.quantity = quantity;}

    public Integer getSystemNotificationsCnt() {return systemNotificationsCnt;}
    public void setSystemNotificationsCnt(Integer systemNotificationsCnt) {
        this.systemNotificationsCnt = systemNotificationsCnt;
    }
}
