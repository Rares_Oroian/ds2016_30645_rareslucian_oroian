package assignment.two.server.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import assignment.two.common.serviceInterfaces.ISellService;
import assignment.two.common.serviceInterfaces.ITaxService;
import assignment.two.server.services.SellService;
import assignment.two.server.services.TaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 24, 2015
 * @Description:
 *	Server application starting point.
 */
public class Server {
	private static final Log LOGGER = LogFactory.getLog(Server.class);
	static Registry registry;
	static ISellService sellServiceStub;
	static ITaxService taxServiceStub;
	static TaxService taxService;
	static SellService sellService;
	
	public static void main(String[] args) {		
		try {
			registry = LocateRegistry.createRegistry(1099);
			System.out.println("RMI registry ready.");
			
			sellService = new SellService();
			sellServiceStub = (ISellService) UnicastRemoteObject.exportObject(sellService, 0);
			registry.rebind("ISellService", sellServiceStub);
			System.out.println("Registered ISellService");
			
			taxService = new TaxService();
			taxServiceStub = (ITaxService) UnicastRemoteObject.exportObject(taxService, 0);
			registry.rebind("ITaxService", taxServiceStub);
			System.out.println("Registered ITaxService");
		} catch (Exception e) {
		    LOGGER.error(e);
		    e.printStackTrace();
		}
	}
}
