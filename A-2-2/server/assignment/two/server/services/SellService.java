package assignment.two.server.services;

import assignment.two.common.entities.Car;
import assignment.two.common.serviceInterfaces.ISellService;

public class SellService implements ISellService {
	
	@Override
	public double computeSellingPrice(Car c) {
		double purchasePrice = c.getPrice();
		double year = c.getYear();
		
		return purchasePrice - (purchasePrice / 7)*(2015-year);
	}
}
