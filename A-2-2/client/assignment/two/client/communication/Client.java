package assignment.two.client.communication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;

import assignment.two.client.gui.SimpleClientGUI;
import assignment.two.common.entities.*;
import assignment.two.common.serviceInterfaces.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description: Starting point of the Client application.
 */
public class Client {
	private static final Log LOGGER = LogFactory.getLog(Client.class);
	private static SimpleClientGUI gui;
	private static String host = "localhost";

	public static void main(String[] args) throws IOException {
		gui = new SimpleClientGUI();
		
		gui.onSubmit(new ActionListener() {
			public void actionPerformed(ActionEvent event)
			{
				try {
					double buyingPrice = Double.parseDouble(gui.getBuyingPrice());
					int manufacturingYear = Integer.parseInt(gui.getYear());
					int engineSize = Integer.parseInt(gui.getEngineSize());
					
					Car car = new Car(manufacturingYear, engineSize, buyingPrice);
					
					double taxValue = requestServerTaxCompunation(car);
					double sellPrice = requestServerSellPriceCompunation(car);
					NumberFormat formatter = new DecimalFormat("#0.00");

					gui.setTax(formatter.format(taxValue));
					gui.setSellingPrice(formatter.format(sellPrice));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		runGUI();
	}
	
	public static double requestServerTaxCompunation(Car car) {
		double taxValue = 0;
		
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			
			ITaxService taxService = (ITaxService) registry.lookup("ITaxService");
			taxValue = taxService.computeTax(car);
		} catch (Exception e) {
			LOGGER.error("", e);
		}
		
		return taxValue;
	}
	
	public static double requestServerSellPriceCompunation(Car car) {
		double sellPrice = 0;
		 
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			
			ISellService sellService = (ISellService)registry.lookup("ISellService");
			sellPrice = sellService.computeSellingPrice(car);
		} catch (Exception e) {
			LOGGER.error("", e);
		}
		
		return sellPrice;
	}
	
	public static void runGUI() {
		SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                gui.displayGUI();
            }
        });
	}
}
