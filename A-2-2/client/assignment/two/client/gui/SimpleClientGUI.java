package assignment.two.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SimpleClientGUI {
	private ActionListener submitActionListener;
	private JLabel taxValueLabel;
	private JLabel sellPriceValueLabel;
	private JTextField carYearText;
	private JTextField carEngineSizeText;
	private JTextField carPriceText;
	
	public SimpleClientGUI() {}
	
	public String getYear() {
		return carYearText.getText();
	}
	
	public String getEngineSize() {
		return carEngineSizeText.getText();
	}
	
	public String getBuyingPrice() {
		return carPriceText.getText();
	}
	
	public void setTax(String tax) {
		taxValueLabel.setText(tax);
	}
	
	public void setSellingPrice(String price) {
		sellPriceValueLabel.setText(price);
	}
	
	public void onSubmit(ActionListener submitActionListener) {
		this.submitActionListener = submitActionListener;
	}
	
	public void displayGUI() {
		LinkedList<Component> components = new LinkedList<Component>();
		
		JFrame frame = new JFrame("Simple client UI");
		frame.setSize(800, 680);
		frame.setLocation(500, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);
		
		JLabel yearLabel = new JLabel("Car manufacturing year:");
		yearLabel.setLocation(100, 100);
		yearLabel.setSize(300, 100);
		components.push(yearLabel);
		
		JLabel priceLabel = new JLabel("Car price:");
		priceLabel.setLocation(100, 300);
		priceLabel.setSize(300, 100);
		components.push(priceLabel);
		
		JLabel engineSizeLabel = new JLabel("Car engine size:");
		engineSizeLabel.setLocation(100, 200);
		engineSizeLabel.setSize(300, 100);
		components.push(engineSizeLabel);
		
		JButton submitButton = new JButton("Calculate");
		submitButton.setSize(220, 30);
        submitButton.setLocation(100, 400);
        submitButton.addActionListener(submitActionListener);
        components.push(submitButton);
        
        JLabel taxLabel = new JLabel("Computed tax:");
        taxLabel.setLocation(100, 450);
		taxLabel.setSize(300, 100);
		components.push(taxLabel);
		
		taxValueLabel = new JLabel("");
		taxValueLabel.setLocation(270, 450);
        taxValueLabel.setSize(300, 100);
        taxValueLabel.setForeground(Color.BLUE);
		components.push(taxValueLabel);
		
		JLabel sellPriceLabel = new JLabel("Sell price:");
		sellPriceLabel.setLocation(400, 450);
        sellPriceLabel.setSize(300, 100);
		components.push(sellPriceLabel);
		
		sellPriceValueLabel = new JLabel("");
		sellPriceValueLabel.setLocation(570, 450);
		sellPriceValueLabel.setSize(300, 100);
		sellPriceValueLabel.setForeground(Color.BLUE);
		components.push(sellPriceValueLabel);
		
		carYearText = new JTextField(40);
		carYearText.setLocation(400, 130);
		carYearText.setSize(270, 40);
		components.push(carYearText);
		
		carEngineSizeText = new JTextField(40);
		carEngineSizeText.setLocation(400, 230);
		carEngineSizeText.setSize(270, 40);
		components.push(carEngineSizeText);
		
		carPriceText = new JTextField(40);
		carPriceText.setLocation(400, 330);
		carPriceText.setSize(270, 40);
		components.push(carPriceText);
		
		for (Component c: components) {
			c.setFont(new Font("Comic Sans MS", 0, 22));
			contentPane.add(c, BorderLayout.CENTER);
		}

		frame.setContentPane(contentPane);
		frame.setVisible(true);
	}
}
