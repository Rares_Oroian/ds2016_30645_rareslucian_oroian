package assignment.two.common.serviceInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import assignment.two.common.entities.Car;

public interface ISellService extends Remote {
	double computeSellingPrice(Car c) throws RemoteException;
}
