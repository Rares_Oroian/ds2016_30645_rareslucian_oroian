package assignment.two.common.serviceInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import assignment.two.common.entities.Car;

public interface ITaxService extends Remote {
	double computeTax(Car c) throws RemoteException;
}
